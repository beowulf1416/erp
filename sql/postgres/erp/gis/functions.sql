set schema 'gis';

create or replace function features_get_all (    
)
returns refcursor
as $$
    declare rc refcursor;
    begin
        open rc for
        select f_table_name
        from public.geometry_columns;

        return rc;
    end;
$$
language plpgsql;