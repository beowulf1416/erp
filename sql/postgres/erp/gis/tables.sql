create table object_types (
    id bigserial,
    name varchar(100) not null,
    constraint pk_object_types primary key (id),
    constraint u_object_types unique (name)
);


create table objects (
    id bigserial,
    type_id bigint not null,
    constraint pk_objects primary key (id),
    constraint fk_objects_1 foreign key (type_id) references object_types(id) on delete restrict
);


create table object_points (
    id bigserial,
    object_id bigint not null,
    x1 decimal(12,5) not null,
    y1 decimal(12,5) not null,
    x2 decimal(12,5) not null,
    y2 decimal(12,5) not null,
    constraint fk_object_points_1 foreign key (object_id) references objects(id) on delete restrict
);


create table layers (
    id bigserial,
    name varchar(100) not null,
    constraint pk_layers primary key (id),
    constraint u_layers_1 unique (name)
);


create table base (
    id bigserial,
    geom public.geometry not null,
    constraint pk_base primary key (id)
);


create table roads (
    id bigserial,
    name varchar(100) not null,
    geom public.geometry not null,
    constraint pk_roads primary key (id)
);


create table pole_types (
    id int,
    name varchar(100) not null,
    constraint pk_pole_types primary key (id),
    constraint u_pole_types unique (name)
);


create table poles (
    id bigserial,
    type_id int not null,
    point public.geometry not null,
    constraint pk_poles primary key (id),
    constraint fk_poles_1 foreign key (type_id) references pole_types(id) on delete restrict
);


create table power_lines (
    id bigserial,
    p1 public.geometry not null,
    p2 public.geometry not null,
    constraint pk_power_lines primary key (id)
);


create table power_line_connections (
    pl_id bigint,
    p1_cn bigint,
    p2_cn bigint,
    constraint pk_power_line_connections primary key (pl_id),
    constraint u_power_line_connections_1 unique (p1_cn, p2_cn)
);