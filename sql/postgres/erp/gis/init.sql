
-- enable postgis extension
create extension postgis;

create schema gis;
set schema 'gis';

\ir tables.sql