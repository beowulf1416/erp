set schema 'accounting';

create or replace function account_types (

)
returns refcursor
as $$
    declare rc refcursor;
    begin
        open rc for
        select
            id,
            name
        from accounting.account_types;

        return rc;
    end;
$$
language plpgsql;


create or replace function account_tree_add (
    p_client_id accounting.account_tree.client_id%type,
    p_account_id accounting.account_tree.account_id%type,
    p_parent_account_id accounting.account_tree.parent_account_id%type
)
returns void
as $$
    begin
        insert into accounting.account_tree (
            client_id,
            account_id,
            parent_account_id
        ) values (
            p_client_id,
            p_account_id,
            p_parent_account_id
        )
        on conflict do nothing;
    end;
$$
language plpgsql;


create or replace function account_add (
    p_client_id accounting.accounts.client_id%type,
    p_type_id accounting.accounts.type_id%type,
    p_code accounting.accounts.code%type,
    p_name accounting.accounts.name%type,
    p_description accounting.accounts.description%type,
    p_parent_account_id accounting.account_tree.parent_account_id%type
)
returns accounting.accounts.id%type
as $$
    declare tmp_id accounting.accounts.id%type;
    begin
        insert into accounting.accounts (
            client_id,
            type_id,
            code,
            name,
            description
        ) values (
            p_client_id,
            p_type_id,
            p_code,
            p_name,
            p_description
        )
        returning currval(pg_get_serial_sequence('accounting.accounts', 'id')) into tmp_id;

        if p_parent_account_id is not null then
            perform * from accounting.account_tree_add(
                p_client_id,
                tmp_id,
                p_parent_account_id
            );
        end if;

        return tmp_id;
    end;
$$
language plpgsql;


create or replace function account_get (
    p_account_id accounting.accounts.id%type
)
returns refcursor
as $$
    declare rc refcursor;
    begin
        open rc for
        select
            id,
            active,
            created_ts,
            type_id,
            code,
            name,
            description
        from accounting.accounts
        where id = p_account_id;

        return rc;
    end;
$$
language plpgsql;


create or replace function account_tree_get_children (
    p_client_id accounting.account_tree.client_id%type,
    p_parent_account_id accounting.account_tree.parent_account_id%type
)
returns refcursor
as $$
    declare rc refcursor;
    begin
        if p_parent_account_id is null or p_parent_account_id = 0 then
            open rc for
            select
                a.id,
                a.active,
                a.created_ts,
                a.type_id,
                a.code,
                a.name,
                a.description
            from accounting.accounts a
                left outer join accounting.account_tree b on a.id = b.account_id
            where a.client_id = p_client_id 
                and b.parent_account_id is null;
        else 
            open rc for
            select
                a.id,
                a.active,
                a.created_ts,
                a.type_id,
                a.code,
                a.name,
                a.description
            from accounting.accounts a
                left outer join accounting.account_tree b on a.id = b.account_id
            where a.client_id = p_client_id
                and b.parent_account_id = p_parent_account_id;
        end if;

        return rc;
    end;
$$
language plpgsql;