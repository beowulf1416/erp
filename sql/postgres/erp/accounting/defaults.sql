create or replace function set_defaults (

)
returns void
as $$
    declare tmp_client_id bigint;
    declare tmp_acct_id_1 bigint;
    declare tmp_acct_id_2 bigint;
    declare tmp_acct_id_3 bigint;
    begin
        insert into accounting.account_types values
        (1,'Assets','Asset'),
        (2,'Liabilities','Liability'),
        (3,'Owner''s Equity/Net Worth','Owner''s Equity'),
        (4,'Net Income','Revenue'),
        (5,'Sales','Revenue'),
        (6,'Cost of Goods Sold','Expense'),
        (7,'Expenses','Expense');

        tmp_client_id := 1;

        tmp_acct_id_1 := accounting.account_add(tmp_client_id, 1, '1000', 'assets', 'assets', null);
        tmp_acct_id_2 := accounting.account_add(tmp_client_id, 1, '1100', 'cash on hand', 'cash on hand', tmp_acct_id_1);
        tmp_acct_id_3 := accounting.account_add(tmp_client_id, 1, '1110', 'petty cash', 'petty cash', tmp_acct_id_2);
        tmp_acct_id_3 := accounting.account_add(tmp_client_id, 1, '1120', 'general checking', 'general checking', tmp_acct_id_2);
        tmp_acct_id_3 := accounting.account_add(tmp_client_id, 1, '11300', 'payroll checking', 'payroll checking', tmp_acct_id_2);
        tmp_acct_id_2 := accounting.account_add(tmp_client_id, 1, '1200', 'accounts receivable', 'accounts receivable', tmp_acct_id_1);
        tmp_acct_id_3 := accounting.account_add(tmp_client_id, 1, '1210', 'accounts receivable - trade', 'accounts receivable - trade', tmp_acct_id_2);     
    end;
$$
language plpgsql;

select * from set_defaults();

drop function set_defaults();