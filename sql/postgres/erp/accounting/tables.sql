create table account_types (
    id int not null,
    name varchar(50) not null,
    atype varchar(50) not null,
    constraint pk_account_types primary key (id),
    constraint u_account_types_1 unique (name, atype)
);

create table accounts (
    id bigserial,
    client_id bigint not null,
    active boolean not null default true,
    created_ts timestamp without time zone not null default(now() at time zone 'utc'),
    type_id int not null,
    code varchar(100),
    name varchar(100) not null,
    description text,
    constraint pk_accounts primary key (id),
    constraint u_accounts_1 unique (client_id, type_id, name),
    constraint u_accounts_2 unique (client_id, type_id, code),
    constraint fk_accounts_1 foreign key (type_id) references account_types(id) on delete restrict 
);


create table account_tree (
    client_id bigint not null,
    account_id bigint not null,
    parent_account_id bigint not null,
    constraint pk_account_tree primary key (client_id, account_id, parent_account_id),
    constraint fk_account_tree_1 foreign key (client_id) references common.clients(id) on delete restrict,
    constraint fk_account_tree_2 foreign key (account_id) references accounts(id) on delete restrict,
    constraint fk_account_tree_3 foreign key (parent_account_id) references accounts(id) on delete restrict
);