create table people (
    id bigserial,
    client_id bigint not null,
    active boolean not null default true,
    created_ts timestamp without time zone not null default(now() at time zone 'utc'),
    last_name varchar(100),
    first_name varchar(100),
    middle_name varchar(100),
    prefix varchar(10),
    suffix varchar(10),
    name_1 varchar(100),
    name_2 varchar(100),
    is_employee boolean not null default false,
    is_vendor boolean not null default false,
    is_customer boolean not null default false,
    is_representative boolean not null default false,
    constraint pk_people_id primary key (id),
    constraint u_people_1 unique (client_id, last_name, first_name, middle_name, prefix, suffix),
    constraint fk_people_1 foreign key (client_id) references common.clients(id) on delete restrict
);

create table emails (
    id bigserial,
    client_id bigint not null,
    active boolean not null default true,
    created_ts timestamp without time zone not null default(now() at time zone 'utc'),
    people_id bigint not null,
    email public.email_address not null,
    constraint pk_emails primary key (id),
    constraint u_emails_1 unique (client_id, email),
    constraint fk_emails_1 foreign key (client_id) references common.clients(id) on delete restrict,
    constraint fk_emails_2 foreign key (people_id) references people(id) on delete restrict
);