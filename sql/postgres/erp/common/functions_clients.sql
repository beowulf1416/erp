set schema 'common';

create or replace function client_add (
    p_name common.clients.name%type,
    p_desc common.clients.description%type
)
returns common.clients.id%type
as $$
    declare tmp_client_id common.clients.id%type;
    begin
        insert into common.clients (
            name,
            description
        ) values (
            p_name,
            p_desc
        )
        returning currval(pg_get_serial_sequence('common.clients','id')) into tmp_client_id;

        perform * from common.organization_add(
            tmp_client_id,
            concat(p_name, ' root organization'),
            'root organization'
        );

        return tmp_client_id;
    end;
$$
language plpgsql;


create or replace function client_update (
    p_id common.clients.id%type,
    p_active common.clients.active%type,
    p_name common.clients.name%type,
    p_desc common.clients.description%type
)
returns void
as $$
    begin
        update common.clients set
            active = p_active,
            name = p_name,
            description = p_desc
        where id = p_id;
    end;
$$
language plpgsql;


create or replace function clients_get (
)
returns refcursor
as $$
    declare rc refcursor;
    begin
        open rc for
        select 
            id,
            active,
            created_ts,
            name,
            description
        from common.clients;

        return rc;
    end;
$$
language plpgsql;


create or replace function client_get (
    p_name common.clients.name%type
)
returns refcursor
as $$
    declare rc refcursor;
    begin
        open rc for
        select
            id,
            active,
            created_ts,
            name,
            description
        from common.clients
        where name = p_name;

        return rc;
    end;
$$
language plpgsql;


create or replace function clients_user_add (
    p_client_id common.clients.id%type,
    p_user_id security.users.id%type
)
returns void
as $$
    begin
        insert into common.client_users (
            client_id,
            user_id
        ) values (
            p_client_id,
            p_user_id
        )
        on conflict (client_id, user_id) do nothing;
    end;
$$
language plpgsql;


create or replace function user_clients_get (
    p_user_id security.users.id%type
)
returns refcursor
as $$
    declare rc refcursor;
    begin
        open rc for
        select
            a.id,
            a.active,
            a.created_ts,
            a.name,
            a.description
        from common.clients a
        where a.id in (
            select
                distinct b.client_id
            from common.client_users b
            where b.user_id = p_user_id
        );

        return rc;
    end;
$$
language plpgsql;