
create or replace function set_defaults (
)
returns void
as $$
    declare tmp_client_id bigint;
    declare tmp_org_id bigint;
    declare tmp_uom_id bigint;
    begin
        insert into common.uom_types (id, name) values 
        (1, 'quantity'),
        (2, 'length'),
        (3, 'area'),
        (4, 'volume'),
        (5, 'time'),
        (6, 'temperature');

        tmp_client_id := common.client_add('system','default client');

        perform * from common.uom_add(tmp_client_id, 1, 'piece', 'pc');
        perform * from common.uom_add(tmp_client_id, 1, 'box', 'box');
        perform * from common.uom_add(tmp_client_id, 2, 'meter', 'm');
        perform * from common.uom_add(tmp_client_id, 3, 'square meter', 'm²');
        perform * from common.uom_add(tmp_client_id, 4, 'cubic meter', 'm³');
        perform * from common.uom_add(tmp_client_id, 4, 'liter', 'l');
        perform * from common.uom_add(tmp_client_id, 5, 'second', 's');
        perform * from common.uom_add(tmp_client_id, 5, 'minute', 'min');
        perform * from common.uom_add(tmp_client_id, 5, 'hour', 'hr');
        perform * from common.uom_add(tmp_client_id, 6, 'Celsius', '℃');
    end;
$$
language plpgsql;

select * from set_defaults();

drop function set_defaults();