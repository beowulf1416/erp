set schema 'common';

create table clients (
    id bigserial,
    active boolean not null default true,
    created_ts timestamp without time zone not null default(now() at time zone 'utc'),
    name varchar(300) not null,
    description text,
    tax_id varchar(100),
    constraint pk_clients_id primary key (id),
    constraint u_clients_1 unique (id)
);


create table client_users (
    client_id bigint not null,
    user_id bigint not null,
    constraint pk_client_users primary key (client_id, user_id),
    constraint fk_client_users_1 foreign key (client_id) references clients(id) on delete restrict,
    constraint fk_client_users_2 foreign key (user_id) references security.users(id) on delete restrict
);


create table organizations (
    id bigserial,
    active boolean not null default true,
    client_id bigint not null,
    created_ts timestamp without time zone not null default(now() at time zone 'utc'),
    name varchar(200) not null,
    description text,
    constraint pk_organizations_id primary key (id),
    constraint u_organizations_1 unique (name),
    constraint fk_organizations_1 foreign key (client_id) references clients(id)
);


create table org_tree (
    client_id bigint not null,
    org_id bigint not null,
    parent_org_id bigint not null,
    constraint u_org_tree_1 unique (client_id, org_id,parent_org_id),
    constraint fk_org_tree_1 foreign key (client_id) references clients(id) on delete restrict,
    constraint fk_org_tree_2 foreign key (org_id) references organizations(id) on delete restrict,
    constraint fk_org_tree_3 foreign key (parent_org_id) references organizations(id) on delete restrict
);


create table uom_types (
    id int not null,
    name varchar(20) not null,
    constraint pk_uom_types primary key (id)
);


create table uom (
    id bigserial,
    active boolean not null default true,
    client_id bigint not null,
    created_ts timestamp without time zone not null default(now() at time zone 'utc'),
    uom_type_id int not null,
    name varchar(50) not null,
    symbol varchar(20),
    constraint pk_uom primary key (id),
    constraint u_uom_1 unique (client_id, name),
    constraint fk_uom_1 foreign key (client_id) references clients(id) on delete restrict,
    constraint fk_uom_2 foreign key (uom_type_id) references uom_types(id) on delete restrict
);


create table documents (
    id bigserial,
    client_id bigint not null,
    created_ts timestamp without time zone not null default( now() at time zone 'utc'),
    key varchar(100) not null,
    description text,
    url varchar(300) not null,
    constraint pk_documents_1 primary key (id),
    constraint fk_documents_1 foreign key (client_id) references clients(id) on delete restrict
);


create table notes (
    id bigserial,
    client_id bigint not null,
    created_ts timestamp without time zone not null default(now() at time zone 'utc'),
    key varchar(100) not null,
    note text not null,
    constraint pk_notes_id primary key (id),
    constraint fk_notes_1 foreign key (client_id) references clients(id) on delete restrict
);