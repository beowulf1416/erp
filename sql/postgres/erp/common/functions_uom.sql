set schema 'common';

create or replace function uom_types_all (
)
returns refcursor
as $$
    declare rc refcursor;
    begin
        open rc for
        select
            id,
            name
        from common.uom_types;

        return rc;
    end;
$$
language plpgsql;


create or replace function uom_add (
    p_client_id common.uom.client_id%type,
    p_uom_type_id common.uom.uom_type_id%type,
    p_name common.uom.name%type,
    p_symbol common.uom.symbol%type
)
returns void
as $$
    begin
        insert into common.uom (
            client_id,
            uom_type_id,
            name,
            symbol
        ) values (
            p_client_id,
            p_uom_type_id,
            p_name,
            p_symbol
        );
    end;
$$
language plpgsql;


create or replace function uom_update (
    p_uom_id common.uom.id%type,
    p_active common.uom.active%type,
    p_uom_type_id common.uom.uom_type_id%type,
    p_name common.uom.name%type,
    p_symbol common.uom.symbol%type
)
returns void
as $$
    begin
        update common.uom set
            active = p_active,
            uom_type_id = p_uom_type_id,
            name = p_name,
            symbol = p_symbol
        where id = p_uom_id;
    end;
$$
language plpgsql;


create or replace function uom_all (
    p_client_id common.uom.client_id%type
)
returns refcursor
as $$
    declare rc refcursor;
    begin
        open rc for
        select
            id,
            active,
            created_ts,
            uom_type_id,
            name,
            symbol
        from common.uom
        where client_id = p_client_id;

        return rc;
    end;
$$
language plpgsql;