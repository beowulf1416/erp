set schema 'common';

create or replace function organization_add (
    p_client_id common.clients.id%type,
    p_name common.organizations.name%type,
    p_desc common.organizations.description%type
)
returns common.organizations.id%type
as $$
    declare tmp common.clients.id%type;
    begin
        insert into common.organizations (
            client_id,
            name,
            description
        ) values (
            p_client_id,
            p_name,
            p_desc
        )
        returning currval(pg_get_serial_sequence('common.organizations','id')) into tmp;

        return tmp;
    end;
$$
language plpgsql;


create or replace function org_assign_parent (
    p_client_id common.organizations.client_id%type,
    p_org_id common.organizations.id%type,
    p_parent_org_id common.organizations.id%type
)
returns void
as $$
    begin
        insert into common.org_tree (
            client_id,
            org_id,
            parent_org_id
        ) values (
            p_client_id,
            p_org_id,
            p_parent_org_id
        );
    end;
$$
language plpgsql;