drop schema accounting cascade;
drop schema gis cascade;
drop schema people cascade;
drop schema inventory cascade;
drop schema common cascade;
drop schema security cascade;
drop schema www cascade;