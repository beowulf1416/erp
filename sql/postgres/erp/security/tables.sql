create table users (
    id bigserial,
    active boolean not null default false,
    created_ts timestamp without time zone not null default(now() at time zone 'utc'),
    email public.email_address not null,
    pw text not null,
    constraint pk_users_id primary key (id),
    constraint u_users_1 unique (email)
);


create table permissions (
    id bigserial,
    active boolean not null default true,
    created_ts timestamp without time zone not null default(now() at time zone 'utc'),
    key varchar(100) not null,
    description text,
    constraint pk_permissions_id primary key (id),
    constraint u_permissions_1 unique (key)
);


create table roles (
    id bigserial,
    active boolean not null default true,
    created_ts timestamp without time zone not null default(now() at time zone 'utc'),
    name varchar(100) not null,
    description text,
    constraint pk_roles_id primary key (id),
    constraint u_roles_1 unique (name)
);


create table user_roles (
    user_id bigint not null,
    role_id bigint not null,
    constraint u_user_roles_1 unique (user_id,role_id),
    constraint fk_user_roles_1 foreign key (user_id) references security.users(id) on delete restrict,
    constraint fk_user_roles_2 foreign key (role_id) references security.roles(id) on delete cascade
);


create table role_permissions (
    role_id bigint not null,
    permission_id bigint not null,
    constraint u_role_permission_1 unique (role_id,permission_id),
    constraint fk_role_permissions_1 foreign key (role_id) references security.roles(id) on delete cascade,
    constraint fk_role_permissions_2 foreign key (permission_id) references security.permissions(id) on delete cascade
);