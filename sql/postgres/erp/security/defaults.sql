create or replace function set_defaults ()
returns void
as $$
    declare tmp_user_id bigint;
    declare tmp_role_id bigint;
    declare tmp_permission_id bigint;
    begin
        -- users
        tmp_user_id := security.user_add('admin@admin.com','testingtime');
        perform * from security.user_set_active(tmp_user_id, true);

        -- roles
        tmp_role_id := security.role_add('sysadmin','system administrator');
        perform * from security.add_role_to_user(tmp_user_id, tmp_role_id);

        -- permissions
        tmp_permission_id := security.permission_add('view.admin','allow user access to admin pages');
        perform * from security.add_permission_to_role(tmp_permission_id, tmp_role_id);

        tmp_permission_id := security.permission_add('admin.clients.view','allow user to view list of clients');
        perform * from security.add_permission_to_role(tmp_permission_id, tmp_role_id);

        tmp_permission_id := security.permission_add('admin.clients.add','allow user to view add a client');
        perform * from security.add_permission_to_role(tmp_permission_id, tmp_role_id);

        tmp_permission_id := security.permission_add('admin.client.view','allow user to view client information');
        perform * from security.add_permission_to_role(tmp_permission_id, tmp_role_id);

        tmp_permission_id := security.permission_add('admin.client.update','allow user to update client information');
        perform * from security.add_permission_to_role(tmp_permission_id, tmp_role_id);

        tmp_permission_id := security.permission_add('admin.users.view','allow user to view list of users');
        perform * from security.add_permission_to_role(tmp_permission_id, tmp_role_id);

        tmp_permission_id := security.permission_add('admin.user.view','allow user to view user information');
        perform * from security.add_permission_to_role(tmp_permission_id, tmp_role_id);

        -- organizations
        tmp_permission_id := security.permission_add('organizations.uoms.view', 'allow user to view units of measure');
        perform * from security.add_permission_to_role(tmp_permission_id, tmp_role_id);

        tmp_permission_id := security.permission_add('organizations.uoms.add', 'allow user to add units of measure');
        perform * from security.add_permission_to_role(tmp_permission_id, tmp_role_id);

        -- accounting
        tmp_permission_id := security.permission_add('accounting.account.add', 'allow user to add account');
        perform * from security.add_permission_to_role(tmp_permission_id, tmp_role_id);

        tmp_permission_id := security.permission_add('accounting.account.view', 'allow user to view accounts');
        perform * from security.add_permission_to_role(tmp_permission_id, tmp_role_id);

        -- inventory
        tmp_permission_id := security.permission_add('inventory.items.view', 'allow user to view Inventory items');
        perform * from security.add_permission_to_role(tmp_permission_id, tmp_role_id);

        tmp_permission_id := security.permission_add('inventory.item.add', 'allow user to view Inventory Item');
        perform * from security.add_permission_to_role(tmp_permission_id, tmp_role_id);

        tmp_permission_id := security.permission_add('inventory.warehouses.view', 'allow user to view Inventory Warehouses');
        perform * from security.add_permission_to_role(tmp_permission_id, tmp_role_id);
    end;
$$
language plpgsql;

select * from set_defaults();

drop function set_defaults();
