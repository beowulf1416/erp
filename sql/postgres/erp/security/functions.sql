-- security functions
set schema 'security';

create or replace function user_add(
    p_email public.email_address,
    p_pw security.users.pw%type
)
returns security.users.id%type
as $$
    declare tmp_id security.users.id%type;
    begin
        insert into security.users (
            email,
            pw
        ) values (
            p_email,
            public.crypt(p_pw, public.gen_salt('md5'))
        )
        returning currval(pg_get_serial_sequence('security.users','id')) into tmp_id;

        perform * from common.clients_user_add(1, tmp_id);

        return tmp_id;
    end;
$$
language plpgsql;


create or replace function user_password_change(
    p_user_id security.users.id%type,
    p_pw security.users.pw%type
)
returns void
as $$
    begin
        update security.users set
            pw = public.crypt(p_pw, public.gen_salt('md5'))
        where id = p_user_id;
    end;
$$
language plpgsql;


create or replace function user_set_active(
    p_user_id security.users.id%type,
    p_active security.users.active%type
)
returns void
as $$
    begin
        update security.users set
            active = p_active
        where id = p_user_id;
    end;
$$
language plpgsql;


create or replace function user_authenticate(
    p_email security.users.email%type,
    p_pw security.users.pw%type
)
returns boolean
as $$
    declare tmp boolean;
    begin
        select
            pw = public.crypt(p_pw, pw)
            into
            tmp
        from security.users
        where email = p_email
            and active = true;

        return tmp; 
    end;
$$
language plpgsql;


create or replace function user_get (
    p_email security.users.email%type
)
returns security.users.id%type
as $$
    declare tmp security.users.id%type;
    begin
        select
            id
            into
            tmp
        from security.users
        where email = p_email;
        
        return tmp;
    end;
$$
language plpgsql;


create or replace function permission_add (
    p_key permissions.key%type,
    p_desc permissions.description%type
)
returns security.permissions.id%type
as $$
    declare tmp security.permissions.id%type;
    begin
        insert into security.permissions (
            key,
            description
        ) values (
            p_key,
            p_desc
        )
        returning currval(pg_get_serial_sequence('security.permissions','id')) into tmp;

        return tmp;
    end;
$$
language plpgsql;


create or replace function role_add (
    p_key roles.name%type,
    p_desc roles.description%type
)
returns security.roles.id%type
as $$
    declare tmp security.roles.id%type;
    begin
        insert into security.roles (
            name,
            description
        ) values (
            p_key,
            p_desc
        )
        returning currval(pg_get_serial_sequence('security.roles','id')) into tmp;

        return tmp;
    end;
$$
language plpgsql;


create or replace function add_permission_to_role (
    p_permission_id role_permissions.permission_id%type,
    p_role_id role_permissions.role_id%type
)
returns void
as $$
    begin
        insert into security.role_permissions (
            role_id,
            permission_id
        ) values (
            p_role_id,
            p_permission_id
        );
    end;
$$
language plpgsql;


create or replace function remove_permission_from_role (
    p_permission_id role_permissions.permission_id%type,
    p_role_id role_permissions.role_id%type
)
returns void
as $$
    begin
        delete from security.role_permissions
        where role_id = p_role_id
            and permission_id = p_permission_id;
    end;
$$
language plpgsql;


create or replace function add_role_to_user (
    p_user_id user_roles.user_id%type,
    p_role_id user_roles.role_id%type
)
returns void
as $$
    begin
        insert into security.user_roles (
            user_id,
            role_id
        ) values (
            p_user_id,
            p_role_id
        );
    end;
$$
language plpgsql;


create or replace function remove_role_from_user (
    p_user_id user_roles.user_id%type,
    p_role_id user_roles.role_id%type
)
returns void
as $$
    begin
        delete from security.user_roles
        where user_id = p_user_id
            and role_id = p_role_id;
    end;
$$
language plpgsql;


create or replace function get_user_permissions (
    p_user_id users.id%type
)
returns refcursor
as $$
    declare tmp refcursor;
    begin
        open tmp for 
        select 
            distinct a.key
        from security.permissions a
            inner join security.role_permissions b on a.id = b.permission_id
            inner join security.user_roles c on b.role_id = c.role_id
        where c.user_id = p_user_id;
        return tmp;
    end;
$$
language plpgsql;


create or replace function is_allowed (
    p_session_id varchar(64),
    permission security.permissions.key%type
)
returns boolean
as $$
    declare tmp bigint;
    declare p_user_id security.users.id%type;
    begin
        select
            value
            into
            p_user_id
        from www.session_data
        where session_id = p_session_id
            and key = 'user-id';

        select
            count(*)
            into
            tmp
        from security.permissions a
            inner join security.role_permissions b on a.id = b.permission_id
            inner join security.user_roles c on b.role_id = c.role_id
        where c.user_id = p_user_id
            and a.key = permission;

        return tmp > 0;
    end;
$$
language plpgsql;


create or replace function get_users (
)
returns refcursor
as $$
    declare rc refcursor;
    begin
        open rc for
        select 
            a.id,
            a.active,
            a.created_ts,
            a.email
        from security.users a;

        return rc;
    end;
$$
language plpgsql;


create or replace function get_roles (
)
returns refcursor
as $$
    declare rc refcursor;
    begin
        open rc for
        select
            a.id,
            a.active,
            a.created_ts,
            a.name
        from security.roles a;

        return rc;
    end;
$$
language plpgsql;


create or replace function get_permissions (
)
returns refcursor
as $$
    declare rc refcursor;
    begin
        open rc for
        select
            a.id,
            a.active,
            a.created_ts,
            a.key
        from security.permissions a;

        return rc;
    end;
$$
language plpgsql;