\i www/functions.sql

\i security/functions.sql

\i common/functions_clients.sql
\i common/functions_organizations.sql
\i common/functions_uom.sql

\i people/functions.sql

\i accounting/functions.sql
\i accounting/functions_accounts.sql

\i inventory/functions_warehouses.sql
\i inventory/functions_items.sql
\i inventory/functions_physical_inventory.sql

\i gis/functions.sql