-- inventory item functions
set schema 'inventory';

create or replace function item_add(
    p_client_id inventory.items.client_id%type,
    p_name inventory.items.name%type,
    p_desc inventory.items.description%type,
    p_brand inventory.items.brand%type,
    p_model inventory.items.model%type,
    p_sku inventory.items.sku%type,
    p_upc_ean inventory.items.upc_ean%type,
    p_perishable inventory.items.perishable%type,
    p_uom_id inventory.items.uom_id%type
)
returns inventory.items.id%type
as $$
    declare tmp_id inventory.items.id%type;
    begin
        insert into inventory.items (
            client_id,
            active,
            name,
            description,
            brand,
            model,
            sku,
            upc_ean,
            perishable,
            uom_id
        ) values (
            p_client_id,
            true,
            p_name,
            p_desc,
            p_brand,
            p_model,
            p_sku,
            p_upc_ean,
            p_perishable,
            p_uom_id
        )
        returning currval(pg_get_serial_sequence('inventory.items','id')) into tmp_id;

        return tmp_id;
    end;
$$
language plpgsql;


create or replace function search_items(
    p_client_id inventory.items.client_id%type,
    search_string varchar(100)
)
returns refcursor
as $$
    declare rc refcursor;
    begin
        open rc for
        select
            a.id,
            a.name,
            a.description,
            a.upc_ean,
            a.sku,
            a.perishable
        from inventory.items a
        where a.client_id = p_client_id
            and a.name like search_string;

        return rc;
    end;
$$
language plpgsql;