
create or replace function set_defaults (
)
returns void
as $$
    declare tmp_client_id bigint;
    declare tmp_item_id bigint;
    declare tmp_warehouse_id bigint;
    begin
        tmp_client_id := 1;

        tmp_item_id := inventory.item_add(tmp_client_id, 'item 1','item 1 description', 'brand 1', 'model 1', 'sku 1', 'upc 1', true, 1);
        tmp_item_id := inventory.item_add(tmp_client_id, 'item 2','item 2 description', 'brand 2', 'model 2','sku 2', 'upc 2', true, 1);

        tmp_warehouse_id := inventory.warehouse_add(tmp_client_id, 'warehouse 1', 'warehouse 1 description');
        tmp_warehouse_id := inventory.warehouse_add(tmp_client_id, 'warehouse 2', 'warehouse 2 description');
    end;
$$
language plpgsql;


select * from set_defaults();

drop function set_defaults();



