create table warehouses (
    id bigserial,
    client_id bigint not null,
    active boolean not null default true,
    created_ts timestamp without time zone not null default(now() at time zone 'utc'),
    name varchar(200) not null,
    description text,
    constraint pk_warehouses_id primary key (id),
    constraint u_warehouses_1 unique (client_id,name),
    constraint fk_warehouses_1 foreign key (client_id) references common.clients(id) on delete restrict
);


create table locations (
    id bigserial,
    client_id bigint not null,
    active boolean not null default true,
    created_ts timestamp without time zone not null default(now() at time zone 'utc'),
    warehouse_id bigint not null,
    level_id varchar(60),
    aisle_id varchar(60),
    shelf_id varchar(60),
    bin_id varchar(60),
    constraint pk_locations_id primary key (id),
    constraint fk_locations_1 foreign key (warehouse_id) references warehouses(id) on delete restrict,
    constraint fk_locations_2 foreign key (client_id) references common.clients(id) on delete restrict
);


create table items (
    id bigserial,
    client_id bigint not null,
    active boolean not null default true,
    created_ts timestamp without time zone not null default(now() at time zone 'utc'),
    name varchar(300) not null,
    description text,
    brand varchar(100),
    model varchar(100),
    upc_ean varchar(100),
    sku varchar(100),
    uom_id bigint not null,
    perishable boolean not null default false,
    constraint pk_items_id primary key (id),
    constraint u_items_1 unique (client_id, name, brand, model, sku, upc_ean),
    constraint fk_items_1 foreign key (client_id) references common.clients(id) on delete restrict,
    constraint fk_items_2 foreign key (uom_id) references common.uom(id) on delete restrict
);


create table item_balances (
    item_id bigint,
    updated_ts timestamp without time zone not null default(now() at time zone 'utc'),
    critical numeric(12,6) not null,
    balance numeric(12,6) not null,
    constraint pk_item_balances_id primary key (item_id),
    constraint fk_item_balances_1 foreign key (item_id) references items(id) on delete restrict
);


create table item_substitutes (
    client_id bigint not null,
    item_id bigint not null,
    substitute_item_id bigint not null,
    constraint u_item_substitutes_1 unique (client_id, item_id, substitute_item_id),
    constraint fk_item_substitutes_1 foreign key (item_id) references items(id) on delete restrict,
    constraint fk_item_substitutes_2 foreign key (substitute_item_id) references items(id) on delete restrict,
    constraint fk_item_substitutes_3 foreign key (client_id) references common.clients(id) on delete restrict
);


create table item_components (
    client_id bigint not null,
    item_id bigint not null,
    component_item_id bigint not null,
    constraint u_item_components_1 unique (client_id, item_id, component_item_id),
    constraint fk_item_components_1 foreign key (client_id) references common.clients(id) on delete restrict,
    constraint fk_item_components_2 foreign key (item_id) references items(id) on delete restrict,
    constraint fk_item_components_3 foreign key (component_item_id) references items(id) on delete restrict
);


create table item_locations (
    client_id bigint not null,
    created timestamp without time zone not null default(now() at time zone 'utc'),
    updated timestamp without time zone,
    expiry date,
    item_id bigint not null,
    location_id bigint not null,
    batch_id varchar(100),
    serial_id varchar(100),
    uom_id bigint,
    quantity numeric(12,6),
    constraint u_item_locations_1 unique (item_id, location_id),
    constraint fk_item_locations_1 foreign key (item_id) references items(id) on delete restrict,
    constraint fk_item_locations_2 foreign key (location_id) references locations(id) on delete restrict,
    constraint fk_item_locations_3 foreign key (client_id) references common.clients(id) on delete restrict,
    constraint fk_item_locations_4 foreign key (uom_id) references common.uom(id) on delete restrict
);


create table products (
    id bigserial,
    client_id bigint not null,
    active boolean not null default true,
    created_ts timestamp without time zone not null default(now() at time zone 'utc'),
    name varchar(300) not null,
    description text,
    item_id bigint not null,
    constraint pk_products_id primary key (id),
    constraint u_products_1 unique (client_id, name),
    constraint fk_products_1 foreign key (item_id) references items(id) on delete restrict,
    constraint fk_products_2 foreign key (client_id) references common.clients(id) on delete restrict
);


create table suppliers (
    id bigserial,
    client_id bigint not null,
    active boolean not null default true,
    created_ts timestamp without time zone not null default(now() at time zone 'utc'),
    constraint pk_suppliers_id primary key (id),
    constraint fk_suppliers_1 foreign key (client_id) references common.clients(id) on delete restrict
);


create table transaction_types (
    id int
);


create table physical_inv (
    id bigserial,
    client_id bigint not null,
    created timestamp without time zone not null default(now() at time zone 'utc'),
    updated timestamp without time zone,
    posted timestamp without time zone,
    constraint pk_physical_inv primary key (id),
    constraint fk_physical_inv_1 foreign key (client_id) references common.clients(id) on delete restrict
);


create table physical_inv_items (
    pi_id bigint,
    created timestamp without time zone not null default(now() at time zone 'utc'),
    updated timestamp without time zone,
    item_id bigint not null,
    location_id bigint not null,
    uom_id bigint not null,
    quantity numeric(12,6),
    uom_id_actual bigint not null,
    quantity_actual numeric(12,6),
    constraint u_physical_inv_items_1 unique (pi_id, item_id, location_id),
    constraint fk_physical_inv_items_1 foreign key (pi_id) references physical_inv(id) on delete restrict,
    constraint fk_physical_inv_items_2 foreign key (location_id) references locations(id) on delete restrict,
    constraint fk_physical_inv_items_3 foreign key (uom_id) references common.uoms(id) on delete restrict,
    constraint fk_physical_inv_items_4 foreign key (uom_id_actual) references common.uoms(id) on delete restrict
);