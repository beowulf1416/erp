-- inventory item functions
set schema 'inventory';

create or replace function warehouse_add (
    p_client_id inventory.warehouses.client_id%type,
    p_name inventory.warehouses.name%type,
    p_description inventory.warehouses.description%type
)
returns inventory.warehouses.id%type
as $$
    declare tmp_id inventory.warehouses.id%type;
    begin
        insert into inventory.warehouses (
            client_id,
            name,
            description
        ) values (
            p_client_id,
            p_name,
            p_description
        )
        returning currval(pg_get_serial_sequence('inventory.warehouses','id')) into tmp_id;

        return tmp_id;
    end;
$$
language plpgsql;