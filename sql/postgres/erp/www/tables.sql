create table sessions (
    id varchar(64) not null,
    user_agent varchar(200),
    language varchar(100),
    remote_addr varchar(20),
    created_ts timestamp without time zone not null default(now() at time zone 'utc'),
    constraint pk_sessions_id primary key (id)
);

create table session_data (
    session_id varchar(64),
    key varchar(200),
    value text,
    constraint u_session_data_1 unique (session_id, key),
    constraint fk_session_data_1 foreign key (session_id) references sessions(id) on delete cascade
);
