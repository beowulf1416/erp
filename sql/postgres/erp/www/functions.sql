set schema 'www';

create or replace function session_create (
    p_session_id www.sessions.id%type,
    p_user_agent www.sessions.user_agent%type,
    p_language www.sessions.language%type,
    p_remote_addr www.sessions.remote_addr%type
)
returns void
as $$
    begin
        insert into www.sessions (
            id,
            user_agent,
            language,
            remote_addr
        ) values (
            p_session_id,
            p_user_agent,
            p_language,
            p_remote_addr
        );
    end;
$$
language plpgsql;


create or replace function session_destroy (
    session_id www.sessions.id%type
)
returns void
as $$
    begin
        delete from www.sessions
        where id = session_id;
    end;
$$
language plpgsql;


create or replace function session_get_user (
    session_id www.sessions.id%type
)
returns bigint
as $$
    declare tmp bigint;
    begin
        select 
            value
            into 
            tmp
        from www.session_data
        where id = session_id
            and key = 'user-id';

        return tmp;
    end;
$$
language plpgsql;


create or replace function session_put (
    p_session_id sessions.id%type,
    p_key session_data.key%type,
    p_value session_data.value%type
)
returns void
as $$
    begin
        insert into www.session_data (
            session_id,
            key,
            value
        ) values (
            p_session_id,
            p_key,
            p_value
        )
        on conflict (session_id, key) do
        update set
            value = p_value;
    
    end;
$$
language plpgsql;


create or replace function session_get (
    p_session_id sessions.id%type,
    p_key session_data.key%type
)
returns session_data.value%type
as $$
    declare tmp www.session_data.value%type;
    begin
        select
            value
            into
            tmp
        from www.session_data
        where session_id = p_session_id
            and key = p_key;

        return tmp;
    end;
$$
language plpgsql;


create or replace function session_remove (
    p_session_id sessions.id%type,
    p_key session_data.key%type
)
returns void
as $$
    begin
        delete from www.session_data
        where session_id = p_session_id
            and key = p_key;
    end;
$$
language plpgsql;