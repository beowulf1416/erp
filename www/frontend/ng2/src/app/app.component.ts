import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'div[id=root]',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  routes = [];

  constructor(
    private title_service: Title
  ) {}

  ngOnInit() {
    this.title_service.setTitle('ERP | Welcome');
  }
}
