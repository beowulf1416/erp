export class ApiResult {
    constructor(
        public status: boolean,
        public errors: any[],
        public messages: string[],
        public data: any
    ) {}
}
