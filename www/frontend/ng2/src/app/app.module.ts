import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { RoutingModule } from './routing.module';

import { ContactUsComponent } from './components/contactus/contactus.component';
import { AboutUsComponent } from './components/aboutus/aboutus.component';
import { HelpComponent } from './components/help/help.component';

// dashboard module
import { DashboardModule } from './dashboard/dashboard.module';
import { SearchComponent } from './search/search.component';
import { AuthService } from './auth.service';
import { WindowRef } from './windowref';
import { NotificationService } from './notification.service';

import { AuthenticationInterceptor } from './authenticationinterceptor';

@NgModule({
  declarations: [
    AppComponent,
    ContactUsComponent,
    AboutUsComponent,
    HelpComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RoutingModule,
    DashboardModule
  ],
  exports: [
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthenticationInterceptor,
      multi: true
    },
    AuthService,
    WindowRef,
    NotificationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
