import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs/Observable';
import { WindowRef } from './windowref';

@Injectable()
export class AuthenticationInterceptor implements HttpInterceptor {
    constructor(
        private auth: AuthService,
        private window: WindowRef
    ) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        console.log('intercept');
        if (this.window.nativeWindow.sessionStorage.length > 0) {
            const session_id = this.window.nativeWindow.sessionStorage.getItem('session-id');
            if (session_id !== '') {
                console.log('intercepting, adding session id');
                request = request.clone({
                    setHeaders: {
                        'session-id': session_id
                    }
                });
            }
        }

        return next.handle(request);
    }
}
