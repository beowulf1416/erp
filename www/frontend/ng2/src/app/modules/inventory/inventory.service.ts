import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiResult } from '../../apiresult';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class InventoryService {

  constructor(
    private http: HttpClient
  ) { }

  get_items(search: any): Observable<ApiResult> {
    return this.http.post<ApiResult>('/api/v1/inventory/items', JSON.stringify({ search: search }));
  }

  get_item(id: string): Observable<ApiResult> {
    return this.http.post<ApiResult>('/api/v1/inventory/item', JSON.stringify({ id: id }));
  }

  add_item(params: any): Observable<ApiResult> {
    return this.http.post<ApiResult>('/api/v1/inventory/item/add', JSON.stringify({ params: params }));
  }

  get_uom_types(): Observable<ApiResult> {
    return this.http.post<ApiResult>('/api/v1/unit/types', JSON.stringify({}));
  }

  get_uom(uom_type_id: number): Observable<ApiResult> {
    return this.http.post<ApiResult>('/api/v1/units', JSON.stringify({ uom_type_id: uom_type_id }));
  }

  get_transactions(params: any): Observable<ApiResult> {
    return this.http.post<ApiResult>('/api/v1/inventory/transactions', JSON.stringify({ params: params }));
  }

  get_transaction(id: number): Observable<ApiResult> {
    return this.http.post<ApiResult>('/api/v1/inventory/transaction', JSON.stringify({ id: id }));
  }

  add_warehouse(params: any): Observable<ApiResult> {
    return this.http.post<ApiResult>('/api/v1/inventory/warehouses/add', JSON.stringify({ params: params }));
  }

  get_warehouses(): Observable<ApiResult> {
    return this.http.post<ApiResult>('/api/v1/inventory/warehouses', JSON.stringify({}));
  }
}
