import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { InventoryService } from '../../inventory.service';

@Component({
  selector: 'app-warehouses',
  templateUrl: './warehouses.component.html',
  styleUrls: ['./warehouses.component.css']
})
export class WarehousesComponent implements OnInit {

  form = {
    alerts: [],
    submitting: false
  };
  warehouses = [];

  constructor(
    private service: InventoryService,
    private title: Title
  ) { }

  ngOnInit() {
    this.title.setTitle('ERP | Warehouses');

  }

}
