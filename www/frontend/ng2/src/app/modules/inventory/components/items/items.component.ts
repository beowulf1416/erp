import { Component, OnInit } from '@angular/core';
import { InventoryService } from '../../inventory.service';
import { Title } from '@angular/platform-browser';
import { Observable } from 'rxjs/Observable';
import { ApiResult } from '../../../../apiresult';
import { Router } from '@angular/router';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  form = {
    submitting: false,
    show_search: false
  };
  search = {
    filter: '',
    sort: {
      column: 'name',
      ascending: true,
    },
    pager: {
      current_page: 1,
      items_per_page: 10
    }
  };
  items = [];
  alerts = [];

  constructor(
    private inv_service: InventoryService,
    private router: Router,
    private title_service: Title
  ) { }

  ngOnInit() {
    this.title_service.setTitle('ERP | Inventory Items');
    this.items_search();
  }

  set_items_per_page(items_per_page: number) {
    this.search.pager.items_per_page = items_per_page;
    this.items_search();
    return false;
  }

  page_first() {
    this.search.pager.current_page = 1;
    this.items_search();
    return false;
  }

  page_last() {
    return false;
  }

  page_previous() {
    if (this.search.pager.current_page > 1) {
      this.search.pager.current_page = this.search.pager.current_page--;
    }
    return false;
  }

  page_next() {
    this.search.pager.current_page = this.search.pager.current_page++;
    return false;
  }

  toggle_search() {
    this.form.show_search = !this.form.show_search;
  }

  items_search() {
    this.inv_service.get_items(this.search).subscribe((r: ApiResult) => {
      if (r.status) {
        this.items = r.data.items;
      } else {
        this.alerts = this.alerts.concat(r.errors);
      }
    }, console.error);
    return false;
  }

  items_sort(column: string) {
    this.search.sort.column = column;
    this.search.sort.ascending = !this.search.sort.ascending;
    this.items_search();
    return false;
  }

  items_new() {
    this.router.navigateByUrl('/inventory/items/new');
  }
}
