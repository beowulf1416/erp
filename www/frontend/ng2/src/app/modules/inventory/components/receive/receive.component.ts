import { Component, OnInit } from '@angular/core';
import { InventoryService } from '../../inventory.service';

@Component({
  selector: 'app-receive',
  templateUrl: './receive.component.html',
  styleUrls: ['./receive.component.css']
})
export class ReceiveComponent implements OnInit {

  form = {
    submitting: false,
    alerts: []
  };
  transaction = {
    id: '',
    created: '',
    po_id: '',
    items: [{
      id: '',
      name: '',
      description: ''
    }]
  };

  constructor(
    private service: InventoryService
  ) { }

  ngOnInit() {
  }

  receive_item_add() {
    this.transaction.items.push({
      id: '',
      name: '',
      description: ''
    });
  }

  receive_save() {
    console.log(this.transaction);
  }
}
