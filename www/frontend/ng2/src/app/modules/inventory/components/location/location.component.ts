import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.css']
})
export class LocationComponent implements OnInit {

  form = {
    alerts: [],
    submitting: false
  };
  location = {
    id: '',
    active: true,
    warehouse_id: '',
    level_id: '',
    aisle_id: '',
    shelf_id: '',
    bin_id: ''
  };

  constructor() { }

  ngOnInit() {
  }

}
