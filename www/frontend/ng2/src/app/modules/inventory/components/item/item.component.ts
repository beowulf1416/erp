import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

import { Item } from '../../models/item';
import { InventoryService } from '../../inventory.service';
import { Title } from '@angular/platform-browser';
import { ApiResult } from '../../../../apiresult';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

  item = {
    id: '',
    name: '',
    description: '',
    brand: '',
    model: '',
    sku: '',
    upc_ean: '',
    uom_id: '',
    perishable: false,
    substitutes: []
  };
  form = {
    submitting: false,
    alerts: [],
    uom_types: [],
    uom_type_id: 0,
    uoms: []
  };

  constructor(
    private inv_service: InventoryService,
    private title_service: Title,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.set_title('');
    this.inv_service.get_uom_types().subscribe((r: ApiResult) => {
      if (r.status) {
        this.form.uom_types = r.data.uom_types;
      } else {
        this.form.alerts = this.form.alerts.concat(r.errors);
      }
    }, (error: any) => {
      console.error(error);
    });
    this.route.paramMap
      .switchMap((params: ParamMap) => {
        if (params.get('id') == null) {
          return Observable.of(new ApiResult(
            false,
            [{ 'type': 'danger', 'message': 'Item id is required' }],
            [],
            null
          ));
        } else {
          return this.inv_service.get_item(params.get('id'));
        }
      }).subscribe((r: ApiResult) => {
        if (r.status) {
          this.item = r.data.item;
        } else {
          this.form.alerts = this.form.alerts.concat(r.errors);
        }
      }, (error: any) => {
        console.error(error);
      });
  }

  set_title(title: string) {
    if (title === '') {
      this.title_service.setTitle('ERP | Inventory');
    } else {
      this.title_service.setTitle('ERP | Inventory - ' + title);
    }
  }

  uom_type_change() {
    console.log('uom_type_change');
    this.form.submitting = true;
    this.inv_service.get_uom(this.form.uom_type_id).subscribe((r: ApiResult) => {
      if (r.status) {
        this.form.uoms = r.data.uoms;
      } else {
        this.form.alerts = this.form.alerts.concat(r.errors);
      }
      this.form.submitting = false;
    }, (error: any) => {
      console.error(error);
      this.form.submitting = false;
    });
  }

  item_new(): void {
    console.log('TODO ItemComponent::item_new()');
  }

  item_save(): void {
    console.log('TODO ItemComponent::item_save()');
    this.form.submitting = true;
    if (this.item.id === '') {
      this.inv_service.add_item(this.item).subscribe((r: ApiResult) => {
        if (r.status) {
          console.log(r);
        } else {
          console.log(r.errors);
          this.form.alerts = this.form.alerts.concat(r.errors);
        }
        this.form.submitting = false;
      }, (error: any) => {
        this.form.submitting = false;
        console.error(error);
      });
    } else {
      console.log('//TODO update');
      this.form.submitting = false;
    }
  }
}
