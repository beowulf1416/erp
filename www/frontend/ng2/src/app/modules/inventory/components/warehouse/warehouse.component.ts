import { Component, OnInit } from '@angular/core';
import { InventoryService } from '../../inventory.service';
import { Title } from '@angular/platform-browser';
import { ApiResult } from '../../../../apiresult';

@Component({
  selector: 'app-warehouse',
  templateUrl: './warehouse.component.html',
  styleUrls: ['./warehouse.component.css']
})
export class WarehouseComponent implements OnInit {

  form = {
    alerts: [],
    submitting: false
  };
  warehouse = {
    id: '',
    name: '',
    description: ''
  };

  constructor(
    private service: InventoryService,
    private title: Title
  ) { }

  ngOnInit() {
  }

  save() {
    this.form.submitting = true;
    this.service.add_warehouse(this.warehouse).subscribe((r: ApiResult) => {
      if (r.status) {
        this.form.alerts.push({ type: 'success', message: 'Warehouse added' });
      } else {
        this.form.alerts = this.form.alerts.concat(r.errors);
      }
      this.form.submitting = false;
    }, console.error);
  }
}
