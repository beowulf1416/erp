import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhysicalinventoryComponent } from './physicalinventory.component';

describe('PhysicalinventoryComponent', () => {
  let component: PhysicalinventoryComponent;
  let fixture: ComponentFixture<PhysicalinventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhysicalinventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhysicalinventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
