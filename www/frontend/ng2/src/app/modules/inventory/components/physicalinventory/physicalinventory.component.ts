import { Component, OnInit } from '@angular/core';
import { InventoryService } from '../../inventory.service';
import { Title } from '@angular/platform-browser';
import { Observable } from 'rxjs/Observable';
import { ApiResult } from '../../../../apiresult';

// import * as $ from 'jquery';

@Component({
  selector: 'app-physicalinventory',
  templateUrl: './physicalinventory.component.html',
  styleUrls: ['./physicalinventory.component.css']
})
export class PhysicalInventoryComponent implements OnInit {

  form = {
    submitting: false,
    alerts: []
  };
  transaction = {
    id: '',
    created: '',
    updated: '',
    items: []
  };

  constructor(
    private service: InventoryService,
    private title: Title
  ) { }

  ngOnInit() {
    this.title.setTitle('ERP | Physical Inventory');
  }

  add_item() {
    console.log('add_item()');
    this.transaction.items.push({
      'item': {
        'id': '',
        'name': '',
        'description': ''
      },
      'location': {
        'id': '',
        'name': ''
      },
      'quantity': 0,
      'uom_id': '',
      'qty_actual': 0,
      'uom_id_actual': ''
    });
    return false;
  }

  save() {
    console.log('save()');
    return false;
  }

  generate() {
    console.log('generate()');
    return false;
  }

  post() {
    console.log('post()');
    return false;
  }
}
