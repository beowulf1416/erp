import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RoutingModule } from './routing.module';

import { InventoryComponent } from './components/inventory/inventory.component';
import { ItemsComponent } from './components/items/items.component';
import { InventoryService } from './inventory.service';
import { WarehouseComponent } from './components/warehouse/warehouse.component';
import { ItemComponent } from './components/item/item.component';
import { LocationComponent } from './components/location/location.component';
import { WarehousesComponent } from './components/warehouses/warehouses.component';
import { ReceiveComponent } from './components/receive/receive.component';
import { TransactionsComponent } from './components/transactions/transactions.component';
import { PhysicalInventoryComponent } from './components/physicalinventory/physicalinventory.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RoutingModule
  ],
  declarations: [
    InventoryComponent,
    ItemsComponent,
    WarehouseComponent,
    ItemComponent,
    LocationComponent,
    WarehousesComponent,
    ReceiveComponent,
    TransactionsComponent,
    PhysicalInventoryComponent
  ],
  providers: [InventoryService]
})
export class InventoryModule { }
