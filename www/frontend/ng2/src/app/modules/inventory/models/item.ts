export class Item {
    constructor(
        public id: string,
        public name: string,
        public description: string,
        public sku: string,
        public upc_ean: string,
        public substitutes: Item[]
    ) {}
}
