export class Location {
    constructor(
        public id: string,
        public client_id: string,
        public active: boolean,
        public created: Date,
        public warehouse_id: string,
        public level_id: string,
        public shelf_id: string,
        public box_id: string
    ) {}
}
