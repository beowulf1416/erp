export class Warehouse {
    constructor(
        public id: string,
        public client_id: string,
        public active: boolean,
        public created: Date,
        public name: string,
        public address: string
    ) {}
}
