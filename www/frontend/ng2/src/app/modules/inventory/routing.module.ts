import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes, Router } from '@angular/router';
import { AuthService } from '../../auth.service';

import { InventoryComponent } from './components/inventory/inventory.component';
import { ItemsComponent } from './components/items/items.component';
import { ItemComponent } from './components/item/item.component';
import { WarehousesComponent } from './components/warehouses/warehouses.component';
import { TransactionsComponent } from './components/transactions/transactions.component';
import { ReceiveComponent } from './components/receive/receive.component';
import { PhysicalInventoryComponent } from './components/physicalinventory/physicalinventory.component';
import { WarehouseComponent } from './components/warehouse/warehouse.component';

const routes: Routes = [
  {
    path: '',
    component: InventoryComponent,
    children: [
      {
        path: 'items',
        children: [
          {
            path: '',
            component: ItemsComponent,
            canActivate: [AuthService],
            data: { permission: 'inventory.items.view' }
          },
          {
            path: 'new',
            component: ItemComponent,
            canActivate: [AuthService],
            data: { permission: 'inventory.item.add' }
          },
          {
            path: 'item/:id',
            component: ItemComponent
          }
        ]
      },
      {
        path: 'warehouses',
        children: [
          {
            path: '',
            component: WarehousesComponent,
            canActivate: [AuthService],
            data: { permission: 'inventory.warehouses.view' }
          },
          {
            path: 'new',
            component: WarehouseComponent
          }
        ]
      },
      {
        path: 'transactions',
        children: [
          {
            path: '',
            component: TransactionsComponent
          },
          {
            path: 'receive',
            component: ReceiveComponent
          },
          {
            path: 'physical',
            component: PhysicalInventoryComponent
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class RoutingModule { }
