import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoutingModule } from './/routing.module';
import { PurchaseorderComponent } from './components/purchaseorder/purchaseorder.component';
import { MaterialreceiptComponent } from './components/materialreceipt/materialreceipt.component';

@NgModule({
  imports: [
    CommonModule,
    RoutingModule
  ],
  declarations: [PurchaseorderComponent, MaterialreceiptComponent]
})
export class ProcurementModule { }
