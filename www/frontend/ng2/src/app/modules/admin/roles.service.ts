import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiResult } from '../../apiresult';
import { Observable } from 'rxjs/Observable';
import { Role } from './models/role';

@Injectable()
export class RolesService {

  constructor(
    private http: HttpClient
  ) { }

  get_roles() {
    return this.http.post('/api/v1/admin/roles', JSON.stringify({}));
  }

  get_role(name: string): Observable<ApiResult> {
    if (name == null || name === '' ) {
      return Observable.of({
        status: false,
        errors: ['Role name is required'],
        messages: [],
        data: []
      });
    } else {
      return this.http.post<ApiResult>('/api/v1/admin/roles/role', JSON.stringify({ name: name }));
    }
  }

  search_roles(name): Observable<ApiResult> {
    if (name == null || name === '') {
      return Observable.of({
        status: false,
        errors: ['Role name is required'],
        messages: [],
        data: []
      });
    } else {
      return this.http.post<ApiResult>('/api/v1/admin/roles/search', JSON.stringify({ name: name }));
    }
  }

  add_role(name: string, description: string): Observable<ApiResult> {
    if (name == null || name === '') {
      return Observable.of({
        status: false,
        errors: ['Role name is required'],
        messages: [],
        data: []
      });
    } else {
      return this.http.post<ApiResult>('/api/v1/admin/roles/new', JSON.stringify({
        name: name,
        description: description
      }));
    }
  }

  update_role(role: Role): Observable<ApiResult> {
    const errors = [];
    if (role.id == null || role.id === '') {
      errors.push('Role Id is required');
    }
    if (role.name == null || role.name === '') {
      errors.push('Role name is required');
    }
    if (errors.length === 0) {
      return this.http.post<ApiResult>('/api/v1/admin/roles/update', JSON.stringify({
        id: role.id,
        active: role.active,
        name: role.name,
        description: role.description,
        permissions: role.permissions
      }));
    } else {
      return Observable.of({
        status: false,
        errors: errors,
        messages: [],
        data: []
      });
    }
  }

  get_permissions(id: string): Observable<ApiResult> {
    return this.http.post<ApiResult>('/api/v1/admin/roles/role/permissions', JSON.stringify({ id: id }));
  }
}
