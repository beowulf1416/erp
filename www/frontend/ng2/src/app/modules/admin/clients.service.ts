import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiResult } from '../../apiresult';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ClientsService {

  constructor(
    private http: HttpClient
  ) { }

  get_clients(): Observable<ApiResult> {
    return this.http.post<ApiResult>('/api/v1/admin/clients', JSON.stringify({}));
  }

  get_client(name: string): Observable<ApiResult> {
    return this.http.post<ApiResult>('/api/v1/admin/client', JSON.stringify({ id: name }));
  }

  add_client(params): Observable<ApiResult> {
    return this.http.post<ApiResult>('/api/v1/admin/client/add', JSON.stringify({ params: params }));
  }

  update_client(params): Observable<ApiResult> {
    return this.http.post<ApiResult>('/api/v1/admin/client/update', JSON.stringify({ params: params }));
  }
}
