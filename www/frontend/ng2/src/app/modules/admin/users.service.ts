import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { ApiResult } from '../../apiresult';
import { Observable } from 'rxjs/Observable';
import { User } from './models/user';

@Injectable()
export class UsersService {

  constructor(
    private http: HttpClient
  ) { }

  get_users() {
    return this.http.post('/api/v1/admin/users', JSON.stringify({}));
  }

  get_user(email: string) {
    if (email === '') {
      return Observable.of(new ApiResult(
        false,
        ['Email address is required'],
        [],
        null
      ));
    } else {
      return this.http.post<ApiResult>('/api/v1/admin/users/user', JSON.stringify({ email: email }));
    }
  }

  update_user(user: User): Observable<ApiResult> {
    return this.http.post<ApiResult>('/api/v1/admin/users/update', JSON.stringify({
      id: user.id,
      active: user.active,
      email: user.email,
      roles: user.roles
    }));
  }
}
