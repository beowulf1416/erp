import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes, Router } from '@angular/router';
import { AuthService } from '../../auth.service';

import { AdminComponent } from './admin.component';
import { UserComponent } from './components/user/user.component';
import { UsersComponent } from './components/users/users.component';
import { RolesComponent } from './components/roles/roles.component';
import { RoleComponent } from './components/role/role.component';
import { PermissionComponent } from './components/permission/permission.component';
import { PermissionsComponent } from './components/permissions/permissions.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ClientsComponent } from './components/clients/clients.component';
import { ClientComponent } from './components/client/client.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: '',
        component: DashboardComponent
      },
      {
        path: 'users',
        children: [
          { path: '', component: UsersComponent },
          { path: 'new', component: UserComponent },
          { path: 'user/:id', component: UserComponent }
        ]
      },
      {
        path: 'roles',
        children: [
          { path: '', component: RolesComponent },
          { path: 'new', component: RoleComponent },
          { path: 'role/:id', component: RoleComponent }
        ]
      },
      {
        path: 'permissions',
        children: [
          { path: '', component: PermissionsComponent },
          { path: 'new', component: PermissionComponent },
          { path: 'permission/:id', component: PermissionComponent }
        ]
      },
      {
        path: 'clients',
        children: [
          {
            path: '',
            component: ClientsComponent,
            canActivate: [AuthService],
            data: { permission: 'admin.clients.view' }
          },
          {
            path: 'new',
            component: ClientComponent,
            canActivate: [AuthService],
            data: { permission: 'admin.clients.add' }
          },
          {
            path: 'client/:id',
            component: ClientComponent,
            canActivate: [AuthService],
            data: { permission: 'admin.client.view' }
          }
        ]
      }
    ]
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class RoutingModule { }
