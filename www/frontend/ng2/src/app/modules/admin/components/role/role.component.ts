import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { RolesService } from '../../roles.service';
import { Title } from '@angular/platform-browser';
import { ApiResult } from '../../../../apiresult';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { PermissionsService } from '../../permissions.service';
import { Role } from '../../models/role';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css']
})
export class RoleComponent implements OnInit, AfterViewInit {

  @ViewChild('spi')
  spi: ElementRef;

  messages = [];
  role = new Role(
    '',
    false,
    null,
    '',
    '',
    []
  );
  search_permission = '';
  search_results = [];
  form = {
    submitting: false
  };

  constructor(
    private role_service: RolesService,
    private permissions_service: PermissionsService,
    private title_service: Title,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.title_service.setTitle('ERP | Role');
    this.route.paramMap
      .switchMap((params: ParamMap) => {
        if (params.get('id') != null) {
          return this.role_service.get_role(params.get('id'));
        } else {
          return Observable.of({
            status: false,
            errors: [],
            messages: [],
            data: []
          });
        }
      }).subscribe((r: ApiResult) => {
        if (r.status && r.data.length > 0) {
          const rt = r.data[0];
          this.role = new Role(
            rt.id,
            rt.active,
            rt.created,
            rt.name,
            rt.description,
            rt.permissions
          );
        } else {
          console.error(r.errors);
        }
      });
  }

  ngAfterViewInit() {
    // register typeahead
    const permission_search$ = Observable.fromEvent(this.spi.nativeElement, 'keyup').pipe(
      map((e: KeyboardEvent) => (<HTMLInputElement>e.target).value),
      filter(text => text.length > 3),
      debounceTime(10),
      distinctUntilChanged(),
      switchMap((text: string) => {
        return this.permissions_service.search_permissions(text);
      })
    );
    permission_search$.subscribe((r: ApiResult) => {
      if (r.status) {
        this.search_results = r.data;
      } else {
        console.error(r.errors);
      }
    });
  }

  submit() {
    this.form.submitting = true;
    if (this.role.id === '') {
      this.role_service.add_role(this.role.name, this.role.description).subscribe((r: ApiResult) => {
        console.log(r);
        this.form.submitting = false;
      });
    } else {
      this.role_service.update_role(this.role).subscribe((r: ApiResult) => {
        if (r.status) {
          this.messages = r.messages;
        } else {
          console.log(r.errors);
          this.messages = r.errors;
        }
        this.form.submitting = false;
      });
    }
  }

  permission_add(name: string) {
    this.form.submitting = true;
    console.log(name);
    this.permissions_service.get_permission(name).subscribe((r: ApiResult) => {
      if (r.status) {
        const p = r.data[0];
        this.role.permissions.push(p);
      } else {
        console.error(r.errors);
      }
      this.form.submitting = false;
    });
  }

  permission_remove() {
    console.log('permission remove');
  }
}
