import { Component, OnInit } from '@angular/core';
import { PermissionsService } from '../../permissions.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Permission } from '../../models/permission';
import { ApiResult } from '../../../../apiresult';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/switchMap';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-permission',
  templateUrl: './permission.component.html',
  styleUrls: ['./permission.component.css']
})
export class PermissionComponent implements OnInit {

  permission = new Permission(
    0,
    false,
    null,
    '',
    ''
  );
  form = {
    submitting: false
  };

  constructor(
    private permission_service: PermissionsService,
    private title_service: Title,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.title_service.setTitle('ERP | Permission');
    this.route.paramMap
      .switchMap((params: ParamMap) => {
        if (params.get('id') == null) {
          return Observable.of({
            status: false,
            errors: [],
            messages: [],
            data: []
          });
        } else {
          return this.permission_service.get_permission(params.get('id'));
        }
      }).subscribe((r: ApiResult) => {
        if (r.status) {
          this.permission = r.data[0];
        } else {
          console.error(r.errors);
        }
      });
  }

  submit(f) {
    console.log(f.value);
    this.permission_service.add_permission(f.value.name, f.value.description).subscribe((r: any) => {
      console.log(r);
    });
  }
}
