import { Component, OnInit } from '@angular/core';
import { Client } from '../../models/client';
import { ClientsService } from '../../clients.service';
import { ApiResult } from '../../../../apiresult';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {

  form = {
    submitting: false,
    alerts: [],
    clients: []
  };

  constructor(
    private client_service: ClientsService
  ) { }

  ngOnInit() {
    this.client_service.get_clients().subscribe((r: ApiResult) => {
      if (r.status) {
        this.form.clients = r.data.clients;
      } else {
        this.form.alerts.push(r.errors);
      }
    }, (error: any) => {
      this.form.alerts.push(error);
    });
  }

}
