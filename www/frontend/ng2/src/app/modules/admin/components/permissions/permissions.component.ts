import { Component, OnInit, AfterViewInit } from '@angular/core';
import { PermissionsService } from '../../permissions.service';

@Component({
  selector: 'div[id=app-permissions]',
  templateUrl: './permissions.component.html',
  styleUrls: ['./permissions.component.css']
})
export class PermissionsComponent implements OnInit, AfterViewInit {

  permissions = [];
  permission = {};

  constructor(
    private permissions_service: PermissionsService
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.permissions_service.get_permissions().subscribe((r: any) => {
      console.log(r);
      if (r.status) {
        this.permissions = r.permissions;
      }
    });
  }

}
