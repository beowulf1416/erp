import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/observable/fromEvent';
import { UsersService } from '../../users.service';
import { RolesService } from '../../roles.service';
import { User } from '../../models/user';

@Component({
  selector: 'div[id=app-users]',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit, AfterViewInit {

  @ViewChild('role')
  role_input: ElementRef;

  users = [];
  selected_user = false;

  role_to_add = '';
  roles_search = [];
  roles = [];

  constructor(
    private users_service: UsersService,
    private roles_service: RolesService
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.users_service.get_users().subscribe((r: any) => {
      console.log(r);
      if (r.status) {
        this.users = r.users;
        this.selected_user = this.users[0];
      }
    });
  }

  select_user(user): void {
    this.selected_user = user;
  }

  user_update(form): void {
    console.log(form.value);
  }

  role_update(form): void {
    console.log(form.value);
  }

  search_role(value): void {
    if ( value.length > 3 ) {
      // should use switchMap here
      // this.roles_service.search_role(value).subscribe((r: any) => {
      //   if (r.status) {
      //     this.roles_search = r.data;
      //   }
      // });
      // value.debounceTime(500)
      //   .switchMap((v: string) => this.roles_service.search_role(v))
      //   .subscribe((r: any) => {
      //     console.log(r);
      //   });
    }
  }

  // role_add(): void {
  //   console.log(this.role_to_add);
  //   if (this.roles_search.length > 0) {

  //   } else {
  //     this.roles_service.add_role(this.role_to_add, '').subscribe((r: any) => {
  //       console.log(r);
  //     });
  //   }
  // }
}
