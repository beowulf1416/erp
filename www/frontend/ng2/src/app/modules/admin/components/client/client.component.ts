import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { Client } from '../../models/client';
import { ClientsService } from '../../clients.service';
import { ApiResult } from '../../../../apiresult';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {

  form = {
    submitting: false,
    id: '',
    name: '',
    description: '',
    active: true,
    alerts: []
  };

  constructor(
    private client_service: ClientsService,
    private title_service: Title,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  set_title(title: string) {
    this.title_service.setTitle('ERP | Client ' + title);
  }

  ngOnInit() {
    this.set_title('Client');
    this.route.paramMap
      .switchMap((params: ParamMap) => {
        if (params.get('id') == null) {
          return Observable.of(new ApiResult(
            false,
            ['Client name is required'],
            [],
            null
          ));
        } else {
          return this.client_service.get_client(params.get('id'));
        }
      }).subscribe((r: ApiResult) => {
        if (r.status) {
          const c = r.data.client;
          this.form.id = c.id;
          this.form.name = c.name;
          this.form.description = c.description;

          this.set_title('Client - ' + this.form.name);
        } else {
          this.form.alerts.push(r.errors);
        }
      });
  }

  submit(): void {
    this.form.submitting = true;
    if (this.form.id === '') {
      this.client_service.add_client(this.form).subscribe((r: ApiResult) => {
        console.log(r);
        this.form.alerts.push({ type: 'success', message: 'Client added' });
        this.form.submitting = false;
      }, (error: any) => {
        this.form.alerts.push({ type: 'danger', message: error });
        this.form.submitting = false;
      });
    } else {
      this.client_service.update_client(this.form).subscribe((r: ApiResult) => {
        if (r.status) {
          this.form.alerts.push({ type: 'success', message: 'Client updated' });
        } else {
          this.form.alerts.push(r.errors);
        }
        this.form.submitting = false;
      }, (error: any) => {
        console.error(error);
        this.form.submitting = false;
      });
    }
  }
}
