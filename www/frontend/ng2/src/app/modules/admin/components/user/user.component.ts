import { Component, OnInit, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { ApiResult } from '../../../../apiresult';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { UsersService } from '../../users.service';
import { User } from '../../models/user';
import { RolesService } from '../../roles.service';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit, AfterViewInit {

  @ViewChild('sri')
  sri: ElementRef;

  user = new User(
    '',
    false,
    null,
    '',
    '',
    []
  );
  search_role = '';
  search_results = [];
  form = {
    submitting: false
  };

  constructor(
    private users_service: UsersService,
    private roles_service: RolesService,
    private title_service: Title,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.title_service.setTitle('ERP | User');
    this.route.paramMap
      .switchMap((params: ParamMap) => {
        if (params.get('id') == null) {
          return Observable.of(new ApiResult(
            false,
            ['Email address is required'],
            [],
            null
          ));
        } else {
          return this.users_service.get_user(params.get('id'));
        }
      }).subscribe((r: ApiResult) => {
        if (r.status) {
          const ru = r.data[0];
          this.user = new User(
            ru.id,
            ru.active,
            ru.created,
            ru.email,
            '',
            ru.roles
          );
        } else {
          console.error(r.errors);
        }
      });
  }

  ngAfterViewInit() {
    // register typeahead
    const role_search$ = Observable.fromEvent(this.sri.nativeElement, 'keyup').pipe(
      map((e: KeyboardEvent) => (<HTMLInputElement>e.target).value),
      filter(text => text.length > 3),
      debounceTime(10),
      distinctUntilChanged(),
      switchMap((text: string) => {
        return this.roles_service.search_roles(text);
      })
    );
    role_search$.subscribe((r: ApiResult) => {
      if (r.status) {
        this.search_results = r.data;
      } else {
        console.error(r.errors);
      }
    });
  }

  submit() {
    this.form.submitting = true;
    if (this.user.id === '') {
      // TODO
      this.form.submitting = false;
    } else {
      this.users_service.update_user(this.user).subscribe((r: ApiResult) => {
        if (!r.status) {
          console.error(r.errors);
        }
        this.form.submitting = false;
      });
    }
  }

  role_add(name: string) {
    this.roles_service.get_role(name).subscribe((r: ApiResult) => {
      if (r.status) {
        this.user.roles.push(r.data[0]);
      } else {
        console.error(r.errors);
      }
    });
  }

}
