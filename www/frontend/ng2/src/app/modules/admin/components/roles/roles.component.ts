import { Component, OnInit, AfterViewInit } from '@angular/core';
import { RolesService } from '../../roles.service';
import { Title } from '@angular/platform-browser';
import { PermissionsService } from '../../permissions.service';

@Component({
  selector: 'div[id=app-roles]',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css']
})
export class RolesComponent implements OnInit, AfterViewInit {

  roles = [];
  selected_role = false;

  constructor(
    private role_service: RolesService,
    private permission_service: PermissionsService,
    private title_service: Title
  ) { }

  ngOnInit() {
    this.title_service.setTitle('ERP | Roles');
  }

  ngAfterViewInit() {
    this.role_service.get_roles().subscribe((r: any) => {
      console.log(r);
      if (r.status) {
        this.roles = r.roles;
        this.selected_role = this.roles[0];
      }
    });
  }

  select_role(role): void {
    this.selected_role = role;
  }
}
