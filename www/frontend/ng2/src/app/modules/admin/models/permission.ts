export class Permission {
    constructor(
        public id: number,
        public active: boolean,
        public created: Date,
        public name: string,
        public description: string
    ) {}
}
