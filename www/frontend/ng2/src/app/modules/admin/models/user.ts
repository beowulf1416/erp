import { Role } from './role';

export class User {
    constructor(
        public id: string,
        public active: boolean,
        public created: Date,
        public email: string,
        public password: string,
        public roles: Role[]
    ) {}
}
