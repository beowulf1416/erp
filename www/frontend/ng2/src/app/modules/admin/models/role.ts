import { Permission } from './permission';

export class Role {
    constructor(
        public id: string,
        public active: boolean,
        public created: Date,
        public name: string,
        public description: string,
        public permissions: Permission[]
    ) {}
}
