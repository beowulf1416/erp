export class Client {
    constructor(
        public id: string,
        public active: boolean,
        public created: Date,
        public name: string,
        public description: string,
        public tax_id: string
    ) {}
}
