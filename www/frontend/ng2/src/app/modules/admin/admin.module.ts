import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
// import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { RoutingModule } from './routing.module';

import { AdminComponent } from './admin.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserComponent } from './components/user/user.component';
import { UsersComponent } from './components/users/users.component';
import { RolesComponent } from './components/roles/roles.component';
import { RoleComponent } from './components/role/role.component';
import { PermissionsComponent } from './components/permissions/permissions.component';
import { PermissionComponent } from './components/permission/permission.component';
import { ClientsComponent } from './components/clients/clients.component';
import { ClientComponent } from './components/client/client.component';

import { UsersService } from './users.service';
import { RolesService } from './roles.service';
import { PermissionsService } from './permissions.service';

import { AuthenticationInterceptor } from '../../authenticationinterceptor';
import { ClientsService } from './clients.service';

@NgModule({
  imports: [
    CommonModule,
    // HttpClientModule,
    FormsModule,
    RoutingModule
  ],
  exports: [
  ],
  declarations: [
    AdminComponent,
    UserComponent,
    PermissionsComponent,
    UsersComponent,
    RolesComponent,
    RoleComponent,
    PermissionComponent,
    DashboardComponent,
    ClientsComponent,
    ClientComponent
  ],
  providers: [
    PermissionsService,
    UsersService,
    RolesService,
    ClientsService,
    // {
    //   provide: HTTP_INTERCEPTORS,
    //   useClass: AuthenticationInterceptor,
    //   multi: true
    // }
  ]
})
export class AdminModule { }
