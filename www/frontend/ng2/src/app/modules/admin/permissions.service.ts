import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Permission } from './models/permission';
import { ApiResult } from '../../apiresult';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PermissionsService {

  constructor(
    private http: HttpClient
  ) { }

  get_permissions(): Observable<ApiResult> {
    return this.http.post<ApiResult>('/api/v1/admin/permissions', JSON.stringify({}));
  }

  get_permission(key: string): Observable<ApiResult> {
    if (key == null || key === '') {
      return Observable.of({
        status: false,
        errors: ['Permission key is required'],
        messages: [],
        data: []
      });
    } else {
      return this.http.post<ApiResult>('/api/v1/admin/permissions/permission', JSON.stringify({ key: key}));
    }
  }

  search_permissions(name: string): Observable<ApiResult> {
    if (name == null || name === '') {
      return Observable.of({
        status: false,
        errors: ['Permission name is required'],
        messages: [],
        data: []
      });
    } else {
      return this.http.post<ApiResult>('/api/v1/admin/permissions/search', JSON.stringify({ name: name }));
    }
  }

  add_permission(name: string, description: string): Observable<ApiResult> {
    if (name == null || name === '') {
      return Observable.of({
        status: false,
        errors: ['Permission name is required'],
        messages: [],
        data: []
      });
    } else {
      return this.http.post<ApiResult>('/api/v1/admin/permissions/new', JSON.stringify({ name: name, description: description }));
    }
  }
}
