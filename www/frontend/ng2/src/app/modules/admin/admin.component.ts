import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'div[id=app-admin]',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor(
    private title_service: Title
  ) { }

  ngOnInit() {
    this.title_service.setTitle('ERP | Admin');
  }

}
