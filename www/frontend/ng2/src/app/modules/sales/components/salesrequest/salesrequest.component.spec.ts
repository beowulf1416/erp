import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesrequestComponent } from './salesrequest.component';

describe('SalesrequestComponent', () => {
  let component: SalesrequestComponent;
  let fixture: ComponentFixture<SalesrequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesrequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesrequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
