import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoutingModule } from './routing.module';
import { SalesrequestComponent } from './components/salesrequest/salesrequest.component';
import { SalesorderComponent } from './components/salesorder/salesorder.component';
import { SalesinvoiceComponent } from './components/salesinvoice/salesinvoice.component';
import { ShipmentComponent } from './components/shipment/shipment.component';

@NgModule({
  imports: [
    CommonModule,
    RoutingModule
  ],
  declarations: [SalesrequestComponent, SalesorderComponent, SalesinvoiceComponent, ShipmentComponent]
})
export class SalesModule { }
