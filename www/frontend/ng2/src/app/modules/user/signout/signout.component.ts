import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AuthService } from '../../../auth.service';
import { Router } from '@angular/router';
import { ApiResult } from '../../../apiresult';

@Component({
  selector: 'app-signout',
  templateUrl: './signout.component.html',
  styleUrls: ['./signout.component.css']
})
export class SignOutComponent implements OnInit, AfterViewInit {

  constructor(
    private auth: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.auth.signout().subscribe((r: ApiResult) => {
      console.log('signout');
      console.log(r);
      this.router.navigate(['/']);
    });
  }
}
