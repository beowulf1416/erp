import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../../auth.service';
import { Router } from '@angular/router';
import { ApiResult } from '../../../apiresult';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SignInComponent implements OnInit {

  form = {
    email: '',
    pw: ''
  };
  alerts = [];

  constructor(
    private auth: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  signin(form) {
    this.alerts = [];
    if (form.value.email !== '' && form.value.pw !== '') {
      this.auth.signin(form.value.email, form.value.pw).subscribe((r: ApiResult) => {
        if (r.status) {
          this.router.navigateByUrl(this.auth.redirect_url);
        } else {
          this.alerts = this.alerts.concat(r.errors);
        }
      }, (error: any) => {
        console.error(error);
      });
    } else {
      this.alerts.push({ type: 'danger', message: 'Email address and password is required'});
    }
  }

}
