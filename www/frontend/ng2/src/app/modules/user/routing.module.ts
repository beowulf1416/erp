import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { UserComponent } from './user/user.component';
import { SettingsComponent } from './settings/settings.component';
import { SignInComponent } from './signin/signin.component';
import { SignUpComponent } from './signup/signup.component';
import { SignOutComponent } from './signout/signout.component';

const routes: Routes = [
  { path: '', component: UserComponent },
  { path: 'signin', component: SignInComponent },
  { path: 'signout', component: SignOutComponent },
  { path: 'signup', component: SignUpComponent },
  { path: 'settings', component: SettingsComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class RoutingModule { }
