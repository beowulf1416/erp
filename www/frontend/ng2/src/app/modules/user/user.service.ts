import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ApiResult } from '../../apiresult';

@Injectable()
export class UserService {

  constructor(
    private http: HttpClient
  ) { }

  // validate_email(email: string) {
  //   if(!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)){
  //     return Observable.create(observer => {
  //       observer.onNext({
  //         status: false,
  //         errors: ['Invalid email address']
  //       });
  //       observer.onCompleted();
  //     });
  //   }
  //   return this.http.post('/api/v1/user/validate/email', JSON.stringify({ email: email}));
  // }

  signup(email: string, pw: string, pw_confirm: string): Observable<ApiResult> {
    const errors = [];
    if (email === '') {
      errors.push('Email address is required');
    }
    if (pw === '') {
      errors.push('Password is required');
    }
    if (pw !== pw_confirm) {
      errors.push('Passwords do not match');
    }
    if (errors.length === 0) {
      return this.http.post<ApiResult>('/api/v1/user/signup', JSON.stringify({ email: email, password: pw, password_confirm: pw_confirm }));
    }
  }

  // signin(email: string, pw: string): Observable<ApiResult> {
  //   if (email === '' || pw === '') {
  //     return Observable.of({
  //       status: false,
  //       errors: ['Email address and password is required'],
  //       messages: [],
  //       data: []
  //     });
  //   } else {
  //     return this.http.post<ApiResult>('/api/v1/user/signin', JSON.stringify({ email: email, password: pw }));
  //   }
  // }
}
