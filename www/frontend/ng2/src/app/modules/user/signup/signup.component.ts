import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignUpComponent implements OnInit {

  form = {
    email: '',
    pw: ''
  };
  alerts = [];

  constructor(
    private user: UserService
  ) { }

  ngOnInit() {
  }

  validate_email(email) {
    return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email);
  }

  signup(form) {
    this.alerts = [];
    if (this.validate_email(form.value.email)) {
      const pw = form.value.pw;
      const pw_confirm = form.value.pw_confirm;
      if (pw != '' && pw == pw_confirm) {
        this.user.signup(form.value.email, form.value.pw, form.value.pw_confirm).subscribe( r => {
          console.log(r);
        });
      } else {
        this.alerts.push({type: 'alert-danger', msg: 'Passwords should not be empty and should match'});
      }
    } else {
      this.alerts.push({type: 'alert-danger', msg: 'Invalid email address'});
    }
  }
}
