import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { UserComponent } from './user/user.component';
import { SettingsComponent } from './settings/settings.component';
import { RoutingModule } from './/routing.module';
import { SignInComponent } from './signin/signin.component';
import { SignUpComponent } from './signup/signup.component';
import { UserService } from './user.service';
import { SignOutComponent } from './signout/signout.component';


@NgModule({
  imports: [
    CommonModule,
    RoutingModule,
    FormsModule,
    HttpClientModule
  ],
  declarations: [
    UserComponent,
    SettingsComponent,
    SignInComponent,
    SignUpComponent,
    SignOutComponent
  ],
  providers: [
    UserService
  ]
})
export class UserModule { }
