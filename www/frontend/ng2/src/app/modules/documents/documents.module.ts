import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoutingModule } from './/routing.module';
import { PurchaseorderComponent } from './components/purchaseorder/purchaseorder.component';

@NgModule({
  imports: [
    CommonModule,
    RoutingModule
  ],
  declarations: [PurchaseorderComponent]
})
export class DocumentsModule { }
