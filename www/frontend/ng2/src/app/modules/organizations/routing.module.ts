import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes, Router } from '@angular/router';
import { AuthService } from '../../auth.service';

import { ClientComponent } from './components/client/client.component';
import { UomsComponent } from './components/uoms/uoms.component';
import { UomComponent } from './components/uom/uom.component';

const routes: Routes = [
  {
    path: '',
    component: ClientComponent,
    children: [
      {
        path: 'uoms',
        children: [
          {
            path: '',
            component: UomsComponent,
          },
          {
            path: 'add',
            component: UomComponent
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class RoutingModule { }
