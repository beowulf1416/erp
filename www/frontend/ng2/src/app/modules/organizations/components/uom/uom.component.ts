import { Component, OnInit } from '@angular/core';
import { OrganizationsService } from '../../organizations.service';
import { Title } from '@angular/platform-browser';
import { ApiResult } from '../../../../apiresult';

@Component({
  selector: 'app-uom',
  templateUrl: './uom.component.html',
  styleUrls: ['./uom.component.css']
})
export class UomComponent implements OnInit {

  form = {
    submitting: false,
    alerts: [],
    id: '',
    active: true,
    uom_types: [],
    uom_type: 0,
    name: '',
    symbol: ''
  };

  constructor(
    private service: OrganizationsService,
    private title: Title
  ) { }

  ngOnInit() {
    this.set_title('');
    this.service.get_unit_types().subscribe((r: ApiResult) => {
      if (r.status) {
        this.form.uom_types = r.data.uom_types;
      } else {
        this.form.alerts.push(r.errors);
      }
    }, (error: any) => {
      console.error(error);
    });
  }

  set_title(title: string) {
    if (this.form.name === '') {
      this.title.setTitle('ERP | UOM');
    } else {
      this.title.setTitle('ERP | UOM - ' + title);
    }
  }

  uom_save() {
    console.log('//TODO save()');
    this.form.submitting = true;
    this.service.add_unit(this.form).subscribe((r: ApiResult) => {
      if (r.status) {
        console.log(r.data);
      } else {
        this.form.alerts.push(r.errors);
      }
      this.form.submitting = false;
    }, (error: any) => {
      console.error(error);
      this.form.submitting = false;
    });
  }
}
