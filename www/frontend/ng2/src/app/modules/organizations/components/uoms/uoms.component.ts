import { Component, OnInit } from '@angular/core';
import { OrganizationsService } from '../../organizations.service';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { ApiResult } from '../../../../apiresult';

@Component({
  selector: 'app-uoms',
  templateUrl: './uoms.component.html',
  styleUrls: ['./uoms.component.css']
})
export class UomsComponent implements OnInit {

  form = {
    submitting: false,
    alerts: [],
    uom_type_id: 0,
    uom_types: [],
    uoms: []
  };

  constructor(
    private service: OrganizationsService,
    private router: Router,
    private title: Title
  ) { }

  ngOnInit() {
    this.title.setTitle('ERP | Organizations');
    this.service.get_unit_types().subscribe((r: ApiResult) => {
      if (r.status) {
        this.form.uom_types = r.data.uom_types;
      } else {
        this.form.alerts.push(r.errors);
      }
    }, (error: any) => {
      console.error(error);
    });
    this.service.get_units(1).subscribe((r: ApiResult) => {
      if (r.status) {
        this.form.uoms = r.data.uoms;
      } else {
        this.form.alerts.push(r.errors);
      }
    }, (error: any) => {
      console.error(error);
    });
  }

  uom_type_change() {
    console.log('uom_type_change');
    this.service.get_units(this.form.uom_type_id).subscribe((r: ApiResult) => {
      if (r.status) {
        this.form.uoms = r.data.uoms;
      } else {
        this.form.alerts.push(r.errors);
      }
    }, (error: any) => {
      console.error(error);
    });
  }

  item_new() {
    console.log('//TODO new()');
    this.router.navigateByUrl('/organizations/uoms/add');
  }

  item_save() {
    console.log('//TODO item_save()');
  }

}
