import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiResult } from '../../apiresult';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class OrganizationsService {

  constructor(
    private http: HttpClient
  ) { }

  get_unit_types(): Observable<ApiResult> {
    return this.http.post<ApiResult>('/api/v1/unit/types', JSON.stringify({}));
  }

  get_units(uom_type_id: number): Observable<ApiResult> {
    return this.http.post<ApiResult>('/api/v1/units', JSON.stringify({ uom_type_id: uom_type_id }));
  }

  add_unit(params: any): Observable<ApiResult> {
    return this.http.post<ApiResult>('/api/v1/units/add', JSON.stringify({ params: params }));
  }

}
