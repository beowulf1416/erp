import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RoutingModule } from './/routing.module';
import { ClientComponent } from './components/client/client.component';
import { OrganizationsService } from './organizations.service';
import { UomsComponent } from './components/uoms/uoms.component';
import { UomComponent } from './components/uom/uom.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RoutingModule
  ],
  declarations: [
    ClientComponent,
    UomsComponent,
    UomComponent
  ],
  providers: [OrganizationsService]
})
export class OrganizationsModule { }
