import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RoutingModule } from './/routing.module';
import { AccountComponent } from './components/account/account.component';
import { ReceivableComponent } from './components/receivable/receivable.component';
import { PayableComponent } from './components/payable/payable.component';
import { AccountingComponent } from './components/accounting/accounting.component';
import { AccountingService } from './accounting.service';
import { InvoiceComponent } from './components/invoice/invoice.component';
import { ChartComponent } from './components/chart/chart.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RoutingModule
  ],
  declarations: [
    AccountComponent,
    ReceivableComponent,
    PayableComponent,
    AccountingComponent,
    InvoiceComponent,
    ChartComponent
  ],
  providers: [
    AccountingService
  ]
})
export class AccountingModule { }
