import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes, Router } from '@angular/router';
import { AuthService } from '../../auth.service';
import { AccountingComponent } from './components/accounting/accounting.component';
import { AccountComponent } from './components/account/account.component';
import { InvoiceComponent } from './components/invoice/invoice.component';
import { ChartComponent } from './components/chart/chart.component';


const routes: Routes = [
  {
    path: '',
    component: AccountingComponent,
    children: [
      {
        path: 'chart',
        component: ChartComponent,
        canActivate: [AuthService],
        data: { permission: 'account_tree_view' }
      },
      {
        path: 'account',
        component: AccountComponent,
        canActivate: [AuthService],
        data: { permission: 'accounting.account.view' },
        children: [
          {
            path: 'new',
            component: AccountComponent,
            canActivate: [AuthService],
            data: { permission: 'accounting.account.add' }
          }
        ]
      },
      {
        path: 'invoice',
        component: InvoiceComponent,
        children: [
          {
            path: 'new',
            component: InvoiceComponent
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class RoutingModule { }
