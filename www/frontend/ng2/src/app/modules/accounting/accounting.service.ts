import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiResult } from '../../apiresult';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AccountingService {

  constructor(
    private http: HttpClient
  ) { }

  account_types(): Observable<ApiResult> {
    return this.http.post<ApiResult>('/api/v1/accounting/types', JSON.stringify({}));
  }

  account_add(params: any): Observable<ApiResult> {
    return this.http.post<ApiResult>('/api/v1/accounting/account/add', JSON.stringify({ params: params }));
  }

  account_get(id: number): Observable<ApiResult> {
    return this.http.post<ApiResult>('/api/v1/accounting/account', JSON.stringify({ id: id }));
  }

  account_tree_children(id: number): Observable<ApiResult> {
    return this.http.post<ApiResult>('/api/v1/accounting/tree', JSON.stringify({ id: id }));
  }

}
