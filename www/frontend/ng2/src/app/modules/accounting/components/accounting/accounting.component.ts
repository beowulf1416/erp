import { Component, OnInit } from '@angular/core';
import { AccountingService } from '../../accounting.service';

@Component({
  selector: 'app-accounting',
  templateUrl: './accounting.component.html',
  styleUrls: ['./accounting.component.css']
})
export class AccountingComponent implements OnInit {

  form = {
    alerts: []
  };
  accounts = [];

  constructor(
    private service: AccountingService
  ) { }

  ngOnInit() {

  }

}
