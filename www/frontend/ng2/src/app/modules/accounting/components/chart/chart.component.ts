import { Component, OnInit } from '@angular/core';
import { AccountingService } from '../../accounting.service';
import { ApiResult } from '../../../../apiresult';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {

  form = {
    alerts: []
  };
  chart = {
    accounts: []
  };

  constructor(
    private service: AccountingService
  ) { }

  ngOnInit() {
    this.service.account_tree_children(0).subscribe((r: ApiResult) => {
      if (r.status) {
        this.chart.accounts = r.data.accounts;
      } else {
        this.form.alerts = this.form.alerts.concat(r.errors);
      }
    }, (error: any) => {
      this.form.alerts.push({ type: 'danger', message: error });
    });
  }

  toggle_expand(acct: any) {
    console.log(acct);
    this.service.account_tree_children(acct.id).subscribe((r: ApiResult) => {
      if (r.status) {
        console.log(this.chart.accounts);
        let a: any = this.account_walk(this.chart.accounts, acct.id);
        console.log(a);
        a.children = r.data.accounts;
      } else {
        this.form.alerts = this.form.alerts.concat(r.errors);
      }
    }, (error: any) => {
      console.error(error);
    });
    return false;
  }

  account_walk(accts: any[], id: number): any {
    return accts.find(acct => {
      console.log('test current account ' + acct.id);
      console.log(acct.id == id);
      if (acct.id == id) {
        return acct.id == id;
      }

      console.log('test children');
      console.log(acct.children);
      if (acct.children) {
        return this.account_walk(acct.children, id);
      }
    });
  }
}
