import { Component, OnInit } from '@angular/core';
import { AccountingService } from '../../accounting.service';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ApiResult } from '../../../../apiresult';


@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  form = {
    submitting: false,
    alerts: [],
    types: [],
    accounts: [],
    title: ''
  };
  account = {
    id: '',
    type_id: '',
    code: '',
    name: '',
    description: '',
    parent_account: {
      id: '',
      name: ''
    }
  };

  constructor(
    private service: AccountingService,
    private title: Title,
    private router: Router
  ) { }

  ngOnInit() {
    this.set_title('');
    this.service.account_types().subscribe((r: ApiResult) => {
      if (r.status) {
        this.form.types = r.data.account_types;
      } else {
        this.form.alerts = this.form.alerts.concat(r.errors);
      }
    }, (error: any) => {
      this.form.alerts.push({ 'type': 'danger', 'message': error });
    });
  }

  set_title(title: string) {
    if (this.form.title === '') {
      this.form.title = 'Account';
      this.title.setTitle('ERP | Account');
    } else {
      this.form.title = 'Account - ' + title;
      this.title.setTitle('ERP | Account - ' + title);
    }
  }

  account_new() {
    this.router.navigateByUrl('/accounting/account/new');
  }

  account_save() {
    this.form.submitting = true;
    this.service.account_add(this.account).subscribe((r: ApiResult) => {
      if (r.status) {
        this.form.alerts.push({ 'type': 'success', 'message': 'Account added' });
      } else {
        this.form.alerts = this.form.alerts.concat(r.errors);
      }
      this.form.submitting = false;
    }, (error: any) => {
      this.form.alerts.push({ 'type': 'danger', 'message': error });
      this.form.submitting = false;
    });
  }

}
