import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, RouterModule } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { ApiResult } from './apiresult';
import { WindowRef } from './windowref';

@Injectable()
export class AuthService implements CanActivate {

  session_id = '';
  permissions = [];
  redirect_url = '';
  session: WindowSessionStorage = null;

  constructor(
    private http: HttpClient,
    private router: Router,
    private windowref: WindowRef
  ) {
    this.session = {
      sessionStorage: this.windowref.nativeWindow.sessionStorage
    };
  }

  is_authenticated(): boolean {
    console.log('checking if authenticated');
    return this.session.sessionStorage.getItem('session-id') !== null;
  }

  is_permitted(permission: string): boolean {
    return this.session.sessionStorage.getItem('permissions').includes(permission);
  }

  signin(email: string, pw: string): Observable<ApiResult> {
    if (email === '' || pw === '') {
      return Observable.of({
        status: false,
        errors: [{ type: 'danger', message: 'Email address and password is required' }],
        messages: [],
        data: []
      });
    } else {
      const self = this;
      const observable = Observable.create(function(observer){
        self.http.post('/api/v1/user/signin', JSON.stringify({ email: email, password: pw }), { observe: 'response' }).subscribe(r => {
          const result: any = r.body;
          if (result.status) {
            self.session_id = r.headers.get('session-id');
            if (self.session_id !== null || self.session_id !== '') {
              self.session.sessionStorage.setItem('session-id', self.session_id);
              self.session.sessionStorage.setItem('permissions', result.data.permissions);
              console.log('session-id set to ' + self.session_id);
            }
          }
          observer.next(result);
          observer.complete();
        }, (error: any) => {
          console.error(error);
        });
      });
      return observable;
    }
  }

  signout(): Observable<ApiResult> {
    const tmp_session_id = this.session_id;
    this.session_id = '';
    this.permissions = [];
    this.session.sessionStorage.clear();
    return this.http.post<ApiResult>('/api/v1/user/signout', JSON.stringify({ session_id: tmp_session_id }));


  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    // console.log(state);
    // console.log(route);
    this.redirect_url = state.url;
    if (this.is_authenticated()) {
      if (this.is_permitted(route.data.permission)) {
        return true;
      } else {
        console.warn('insufficient permissions');
        console.log(route.data.permission);
        return false;
      }
    } else {
      console.warn('not authenticated');
      this.router.navigate(['user', 'signin']);
      return false;
    }
  }
}
