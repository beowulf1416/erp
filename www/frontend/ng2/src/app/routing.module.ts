import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ContactUsComponent } from './components/contactus/contactus.component';
import { AboutUsComponent } from './components/aboutus/aboutus.component';
import { HelpComponent } from './components/help/help.component';
import { AppComponent } from './app.component';
import { AuthService } from './auth.service';


const routes: Routes = [
  {
    path: 'contactus',
    component: ContactUsComponent
  },
  { path: 'aboutus', component: AboutUsComponent },
  { path: 'help', component: HelpComponent },
  { path: 'user', loadChildren: 'app/modules/user/user.module#UserModule' },
  {
    path: 'admin',
    loadChildren: 'app/modules/admin/admin.module#AdminModule',
    canActivate: [AuthService],
    data: { permission: 'view.admin' }
  },
  {
    path: 'organizations',
    loadChildren: 'app/modules/organizations/organizations.module#OrganizationsModule'
  },
  {
    path: 'accounting',
    loadChildren: 'app/modules/accounting/accounting.module#AccountingModule'
  },
  {
    path: 'sales',
    loadChildren: 'app/modules/sales/sales.module#SalesModule'
  },
  {
    path: 'procurement',
    loadChildren: 'app/modules/procurement/procurement.module#ProcurementModule'
  },
  { path: 'inventory', loadChildren: 'app/modules/inventory/inventory.module#InventoryModule' },
  { path: 'work', loadChildren: 'app/modules/work/work.module#WorkModule' },
  { path: '', component: AppComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class RoutingModule { }
