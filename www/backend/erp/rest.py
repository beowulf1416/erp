# package erp
import logging

log = logging.getLogger(__name__)

from pyramid.view import view_config
import sys

# from erp.modules.core.commandregistry import CommandRegistry

# @view_config(
#     route_name='api',
#     request_method='POST',
#     renderer='json'
# )
# def view_rest(request):
#     version = request.matchdict['version']
#     module = request.matchdict['module']
#     predicates = request.matchdict['predicates']

#     log.debug('view_rest version: {version} module: {module} predicates: {predicates}'.format(
#         version = version,
#         module = module,
#         predicates = predicates
#     ))

#     registry = CommandRegistry()
#     handler = registry.get_handler(version, module, predicates)
#     try:
#         # permission = handler['permission'] if 'permission' in handler else ''
#         # if permission == '':
#         #     return handler['handler'](request)
#         # else:
#         #     session_id = request.headers['session-id'] if 'session-id' in request.headers else ''
#         #     log.debug(session_id)
#         #     return handler['handler'](request)
#         return handler(request)
#     except:
#         log.error(sys.exc_info())

#     return {}