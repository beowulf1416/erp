# package erp
import os
import logging
from pyramid.view import view_config
from pyramid.response import FileResponse


log = logging.getLogger(__name__)

@view_config(
    route_name='angular'
)
def view_angular(request):
    base = os.path.dirname(__file__)
    file = request.matchdict['file']
    actual = os.path.join(base, 'static', 'js', file)
    log.debug(actual)
    return FileResponse(actual, request=request)


@view_config(
    route_name='angular_map'
)
def view_angular_map(request):
    base = os.path.dirname(__file__)
    file = request.matchdict['file']
    actual = os.path.join(base, 'static', 'js', '{0}.js.map'.format(file))
    log.debug(actual)
    return FileResponse(actual, request=request)

@view_config(
    route_name='angular_module'
)
def view_angular_module(request):
    base = os.path.dirname(__file__)
    file = request.matchdict['file']
    actual = os.path.join(base, 'static', 'js', '{0}.chunk.js'.format(file))
    log.debug(actual)
    return FileResponse(actual, request=request)