# package erp
import logging

log = logging.getLogger(__name__)

from pyramid.view import view_config

@view_config(
    route_name='home', 
    renderer='templates/index.html.jinja2'
)
@view_config(
    route_name='aboutus',
    renderer='templates/index.html.jinja2'
)
@view_config(
    route_name='contactus',
    renderer='templates/index.html.jinja2'
)
def view_home(request):
    log.debug(request.matched_route.name)
    return {'project': 'erp'}
