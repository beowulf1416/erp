# package erp
from pyramid.config import Configurator

from erp.modules.core.database import Database
import erp.modules.core.session
from erp.modules.core.sessionauthpolicy import SessionAuthenticationPolicy
from erp.modules.core.authorizationpolicy import AuthorizationPolicy

def config_routes(config):
    # config.add_route('api','/api/{version}/{module}/*predicates')

    config.add_route('angular', '/angular/{file}')
    config.add_route('angular_map', '/{file}.js.map')
    config.add_route('angular_module','/{file}.chunk.js')

    config.add_route('home', '/')
    config.add_route('aboutus', '/aboutus')
    config.add_route('contactus', '/contactus')


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    config = Configurator(settings=settings)

    db = Database()
    db.init(config)

    session_factory = erp.modules.core.session.db_session_factory()
    config.set_session_factory(session_factory)

    config.set_authentication_policy(SessionAuthenticationPolicy())
    config.set_authorization_policy(AuthorizationPolicy())

    config.include('pyramid_jinja2')
    config.add_static_view('static', 'static', cache_max_age=3600)

    db = Database()
    db.init(config)

    config_routes(config)
    
    config.scan()
    return config.make_wsgi_app()
