# package erp.modules.core
import logging

log = logging.getLogger(__name__)

import sys
import psycopg2

class Database(object):
    __instance = None
    __settings = None
    __connections = {}
    def __new__(cls):
        if Database.__instance is None:
            Database.__instance = object.__new__(cls)
        return Database.__instance

    def init(self, config):
        self.__settings = config.registry.settings

    def get_connection(self, key = 'default'):
        if key in self.__connections:
            log.debug('checking existing connection')
            cn = self.__connections[key]
            if (cn.closed):
                try:
                    dsn = 'host={0} port={1} dbname={2} user={3} password={4}'.format(
                        self.__settings['db.{0}.host'.format(key)],
                        self.__settings['db.{0}.port'.format(key)],
                        self.__settings['db.{0}.name'.format(key)],
                        self.__settings['db.{0}.user'.format(key)],
                        self.__settings['db.{0}.password'.format(key)]
                    )
                    log.debug('connecting to \'{0}\''.format(dsn))
                    cn = psycopg2.connect(dsn=dsn)
                    self.__connections[key] = cn
                except:
                    log.error(sys.exc_info())
                    self.__connections[key] = False
        else:
            log.debug('creating database connection [{0}]'.format(key))
            try:
                dsn = 'host={0} port={1} dbname={2} user={3} password={4}'.format(
                    self.__settings['db.{0}.host'.format(key)],
                    self.__settings['db.{0}.port'.format(key)],
                    self.__settings['db.{0}.name'.format(key)],
                    self.__settings['db.{0}.user'.format(key)],
                    self.__settings['db.{0}.password'.format(key)]
                )
                log.debug('connecting to \'{0}\''.format(dsn))
                cn = psycopg2.connect(dsn=dsn)
                self.__connections[key] = cn
            except:
                log.error(sys.exc_info())
                self.__connections[key] = False

        return self.__connections[key]