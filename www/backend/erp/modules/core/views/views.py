# package erp.modules.core.views
import logging
from pyramid.view import view_config

log = logging.getLogger(__name__)

@view_config(
    route_name='user',
    renderer='templates/index.html.jinja2'
)
# @view_config(
#     route_name='admin',
#     renderer='templates/index.html.jinja2'
# )
# @view_config(
#     route_name='admin_params',
#     renderer='templates/index.html.jinja2'
# )
# @view_config(
#     route_name='inventory',
#     renderer='templates/index.html.jinja2'
# )
def view_index(request):
    log.debug(request.matched_route.name)
    if 'params' in request.matchdict:
        log.debug(request.matchdict['params'])
    return {}