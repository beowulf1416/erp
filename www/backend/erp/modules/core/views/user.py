# package erp.modules.core.views
import logging
from pyramid.view import view_config

log = logging.getLogger(__name__)


@view_config(
    route_name = 'email_verification',
    request_method = 'GET',
    renderer = 'templates/email-verification.html.jinja2'
)
def view_email_verification(request):
    log.debug(request.matched_route.name)

    token = request.matchdict['token']
    log.debug(token)

    return {}