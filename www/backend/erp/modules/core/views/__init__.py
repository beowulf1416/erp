# package erp.modules.core.views
import logging

log = logging.getLogger(__name__)


def includeme(config):
    log.debug('including erp.modules.core.views')

    # config.add_route('user', '/user')
    # config.add_route('user_signin','/user/signup')
    # config.add_route('user_signup','/user/signin')
    config.add_route('user', '/user/*params')

    # config.add_route('admin', '/admin')
    # config.add_route('admin_params', '/admin/*params')

    # config.add_route('inventory', '/inventory')

    config.add_route('email_verification','/verify/email/{token}')