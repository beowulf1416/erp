# package erp.modules.core.commands
import logging

log = logging.getLogger(__name__)


def includeme(config):
    log.debug('including erp.modules.core.commands')

    config.add_route(
        name='user_signin_command',
        path='/api/v1/user/signin'
    )

    config.add_route(
        name='user_signout_command',
        path='/api/v1/user/signout'
    )

    config.add_route(
        name='user_signup_command',
        path='/api/v1/user/signup'
    )