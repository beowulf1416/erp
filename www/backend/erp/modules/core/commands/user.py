# package erp.modules.core.commands
import logging
import sys
import psycopg2
import re
import json
from pyramid_mailer.message import Message
import uuid, base64
import os

from pyramid.view import view_config


log = logging.getLogger(__name__)

from erp.modules.core.database import Database

@view_config(
    route_name='user_signin_command',
    renderer='json',
    request_method='POST'
)
def user_signin_command(request):
    log.debug('user_signin_command')
    status = False
    errors = []
    params = request.json_body

    permissions = []
    if 'email' not in params:
        errors.append({ 'type': 'warning', 'message': 'Email address is required' })

    if 'password' not in params:
        errors.append({ 'type': 'warning', 'message': 'Password is required' })

    if len(errors) == 0:
        email = params['email']
        pw = params['password']

        sql = 'select security.user_authenticate(%s,%s)'
        cn = Database().get_connection('default') 
        try:
            c = cn.cursor()
            c.execute(sql, (email, pw))
            result = c.fetchall()
            status = result[0][0]
            log.debug('security.user_authenticate returns {0}'.format(status))
            if not status:
                errors.append({ 'type': 'danger', 'message': 'Email and Password combination is incorrect' })
        except:
            log.error(sys.exc_info())
            errors.append({ 'type': 'danger', 'message': 'An error occured during sign in. Unable to authenticate user.' })

        if status:
            user_id = ''
            sql = 'select security.user_get(%s)'
            try:
                c = cn.cursor()
                c.execute(sql, (email, ))
                result = c.fetchall()
                user_id = result[0][0]
                log.debug('security.user_get returns {0}'.format(user_id))
            except:
                log.error(sys.exc_info())
                errors.append({ 'type': 'danger', 'message': 'An error occured during sign in. Unable to retrieve user id' })

            if user_id != '':
                request.session['user-id'] = user_id

                # TODO sql
                # retrieve client_id
                sql = 'select * from common.user_clients_get(%s)'
                try:
                    c = cn.cursor()
                    c.execute(sql, (user_id, ))
                    c_name = c.fetchone()
                    c.execute(f'fetch all in "{c_name[0]}"')
                    result = c.fetchone()
                    request.session['client-id'] = result[0]
                    # log.debug(result)
                    # log.debug(result[0])
                except:
                    log.error(sys.exc_info())
                    errors.append({ 'type': 'danger', 'message': 'An error occured during sign in. Unable to retrieve user client id' })

                # TODO sql
                sql = '''
                select
                    p.id,
                    p.active,
                    p.created_ts,
                    p.key,
                    p.description
                from security.permissions p
                    inner join security.role_permissions rp on p.id = rp.permission_id
                    inner join security.user_roles ur on ur.role_id = rp.role_id
                where ur.user_id = %s
                '''

                # session_permissions = {}
                try:
                    c = cn.cursor()
                    c.execute(sql, (user_id, ))
                    result = c.fetchall()
                    for r in result:
                        permissions.append(r[3])
                except:
                    log.error(sys.exc_info())
                    errors.append({ 'type': 'danger', 'message': 'An error occured during sign in. Unable to retrieve user permissions' })

                request.session['permissions'] = json.dumps(permissions)

    return {
        'status': status,
        'errors': errors,
        'data': {
            # 'session_id': request.session['Session-Id'],
            'permissions': permissions
        }
    }


@view_config(
    route_name='user_signout_command',
    renderer='json',
    request_method='POST'
)
def user_signout_command(request):
    status = False
    errors = []
    params = request.json_body
    session_id = params['session_id'] if 'session_id' in params else ''

    # TODO sql
    sql = '''
    delete from www.sessions
    where id = %s
    '''
    cn = Database().get_connection()
    try:
        c = cn.cursor()
        c.execute(sql, (session_id, ))
        cn.commit()

        status = True
    except:
        cn.rollback()
        log.error(sys.exc_info())
        errors.append({ 'type': 'warning', 'message': 'An error occured during sign out' })

    return {
        'status': status,
        'errors': errors
    }

@view_config(
    route_name='user_signup_command',
    renderer='json',
    request_method='POST'
)
def user_signup_command(request):
    log.debug('user_signup_command')

    params = json.loads(request.body)

    status = False
    email = ''
    pw = ''
    pw_confirm = ''

    errors = []
    if 'email' not in params:
        errors.append('Email address is required')

    if 'password' not in params:
        errors.append('Password is required')

    if 'password_confirm' not in params:
        errors.append('Password confirmation is required')

    email = params['email']
    pw = params['password']
    pw_confirm = params['password_confirm']

    if not re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$", email):
        errors.append('A valid email address is required')

    if pw != pw_confirm:
        errors.append('Passwords should match')

    if len(errors) == 0:
        sql = 'select security.user_add(%s,%s)'
        cn = Database().get_connection('default')
        try:
            c = cn.cursor()
            c.execute(sql, (email, pw))
            cn.commit()
            result = c.fetchall()
            user_id = result[0][0]
            
            token = user_generate_email_verification_token()
            mail = user_signup_mail(
                email = email,
                email_verification_url = request.route_url('email_verification', token = token),
                sender_email = request.registry.settings['email.default']
            )
            request.mailer.send(mail)
            status = True
        except psycopg2.IntegrityError as e:
            cn.rollback()
            log.error(e.pgerror)
            errors.append('An email address similar to the one you provided is already registered')
        except:
            cn.rollback()
            log.error(sys.exc_info())
            errors.append('An error occured during sign up. Please contact the adminstrators.')

    return {
        'status': status,
        'errors': errors
    }

def user_generate_email_verification_token():
    return str(base64.b64encode(uuid.UUID(bytes = os.urandom(16)).bytes))

def user_signup_mail(email, email_verification_url, sender_email):
    body = '''
    Welcome!

    Please click on the below link to verify your email address:
    {0}
    '''.format(email_verification_url)

    return Message(
        subject = 'Email verification',
        sender = sender_email,
        recipients = [email],
        body = body
    )