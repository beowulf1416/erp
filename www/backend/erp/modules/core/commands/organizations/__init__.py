# package erp.modules.core.commands.organizations
import logging

log = logging.getLogger(__name__)

def includeme(config):
    log.debug('including erp.modules.core.commands.organizations')

    config.add_route(
        name='organizations_home_view',
        path='/organizations'
    )

    config.add_route(
        name='organizations_params_view',
        path='/organizations/*params'
    )

    # units
    config.add_route(
        name='organizations_uom_types_view',
        path='/api/v1/unit/types'
    )

    config.add_route(
        name='organizations_uoms_view',
        path='/api/v1/units'
    )

    config.add_route(
        name='organizations_uoms_add_view',
        path='/api/v1/units/add'
    )