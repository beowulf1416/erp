# package erp.modules.core.commands.organizations
from pyramid.view import view_config
from erp.modules.core.database import Database

import sys
import logging

log = logging.getLogger(__name__)


@view_config(
    route_name='organizations_uom_types_view',
    renderer='json',
    request_method='POST'
)
def organizations_uom_types_view(request):
    status = False
    data = {}
    errors = []

    sql = 'select * from common.uom_types_all()'
    cn = Database().get_connection('default')
    try:
        c = cn.cursor()
        c.execute(sql)
        c_name = c.fetchone()
        c.execute(f'fetch all in "{c_name[0]}"')
        result = c.fetchall()
        uom_types = []
        for r in result:
            uom_types.append({
                'id': r[0],
                'name': r[1]
            })
        data['uom_types'] = uom_types
        status = True
    except:
        log.error(sys.exc_info())
        errors.append({
            'type': 'danger',
            'message': 'An error occured while trying to retrieve UOM Types'
        })

    return {
        'status': status,
        'errors': errors,
        'data': data
    }


@view_config(
    route_name='organizations_uoms_view',
    renderer='json',
    request_method='POST',
    permission='organizations.uoms.view'
)
def organizations_uoms_view(request):
    status = False
    data = {}
    errors = []
    # params = request.json_body

    client_id = request.session['client-id']

    sql = 'select * from common.uom_all(%s)'
    cn = Database().get_connection('default')
    try:
        c = cn.cursor()
        c.execute(sql, (client_id, ))
        c_name = c.fetchone()
        c.execute(f'fetch all in "{c_name[0]}"')
        result = c.fetchall()
        uoms = []
        for r in result:
            uoms.append({
                'id': r[0],
                'active': r[1],
                'created': r[2].isoformat(),
                'uom_type_id': r[3],
                'name': r[4],
                'symbol': r[5]
            })
        data['uoms'] = uoms
        status = True
    except:
        log.error(sys.exc_info())
        errors.append({
            'type': 'danger',
            'message': 'An error occured while retrieving units of measure'
        })

    return {
        'status': status,
        'errors': errors,
        'data': data
    }


@view_config(
    route_name='organizations_uoms_add_view',
    renderer='json',
    request_method='POST',
    permission='organizations.uoms.add'
)
def organizations_uoms_add_view(request):
    status = True
    data = {}
    errors = []
    params = request.json_body['params']

    client_id = request.session['client-id']
    uom_type_id = params['uom_type'] if 'uom_type' in params else ''
    name = params['name'] if 'name' in params else ''
    symbol = params['symbol'] if 'symbol' in params else ''

    if uom_type_id == '':
        errors.append({ 'type': 'danger', 'message': 'UOM Type is required' })

    if name == '' or symbol == '':
        errors.append({ 'type': 'danger', 'message': 'Name and Symbol is required' })

    if len(errors) == 0:
        sql = 'select * from common.uom_add(%s,%s,%s,%s)'
        cn = Database().get_connection('default')
        try:
            c = cn.cursor()
            c.execute(sql, (client_id, uom_type_id, name, symbol ))
            cn.commit()
            status = True
        except:
            cn.rollback()
            log.error(sys.exc_info())
            errors.append({ 'type': 'danger', 'message': 'An error occured while trying to add a Unit of Measure' })

    return {
        'status': status,
        'errors': errors,
        'data': data
    }