# package erp.modules.core.commands.organizations
from pyramid.view import view_config

@view_config(
    route_name='organizations_home_view',
    renderer='erp:templates/index.html.jinja2'
)
@view_config(
    route_name='organizations_params_view',
    renderer='erp:templates/index.html.jinja2'
)
def organizations_home_view(request):
    return {}