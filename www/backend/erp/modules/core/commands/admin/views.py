# erp.modules.core.admin
from pyramid.view import view_config


@view_config(
    route_name='admin_home_view',
    renderer='erp:templates/index.html.jinja2'
)
@view_config(
    route_name='admin_params_view',
    renderer='erp:templates/index.html.jinja2'
)
def admin_home_view(request):
    return {}