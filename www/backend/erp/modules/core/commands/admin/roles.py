# package erp.modules.core.commands.admin
from pyramid.view import view_config
import logging
import sys

from erp.modules.core.database import Database

log = logging.getLogger(__name__)


@view_config(
    route_name='admin_roles_command',
    renderer='json',
    request_method='POST'
)
def admin_roles_command(request):
    status = False
    errors = []
    roles = []
    sql = '''
    select
        id,
        active,
        created_ts,
        name,
        description
    from security.roles
    '''
    cn = Database().get_connection()
    try:
        c = cn.cursor()
        c.execute(sql)
        result = c.fetchall()
        for r in result:
            roles.append({
                'id': r[0],
                'active': r[1],
                'created': r[2].isoformat(),
                'name': r[3],
                'description': r[4]
            })
        status = True
    except:
        log.error(sys.exc_info())
        errors.append({ 'msg': 'An error occured while retrieving roles' })

    return {
        'status': status,
        'errors': errors,
        'roles': roles
    }


@view_config(
    route_name='admin_roles_search_command',
    renderer='json',
    request_method='POST'
)
def admin_roles_search_command(request):
    status = False
    errors = []
    data = []
    params = request.json_body
    name = params['name'] if 'name' in params else 'none'
    sql = 'select name from security.roles where name like %s'
    cn = Database().get_connection()
    try:
        c = cn.cursor()
        c.execute(sql, ('%{0}%'.format(name),))
        result = c.fetchall()
        for r in result:
            data.append({
                'name': r[0]
            })
        status = True
    except:
        log.error(sys.exc_info())
        errors.append({ 'msg', 'An error occured while searching for a role'})

    return {
        'status': status,
        'errors': errors,
        'data': data
    }


@view_config(
    route_name='admin_roles_new_command',
    renderer='json',
    request_method='POST'
)
def admin_roles_new_command(request):
    status = False
    errors = []
    params = request.json_body
    name = params['name'] if 'name' in params else ''
    description = params['description'] if 'description' in params else ''
    
    if (name == ''):
        errors.append({ 'msg': 'Role name cannot be empty' })
    else :
        sql = 'select * from security.role_add(%s,%s)'
        cn = Database().get_connection()
        try:
            c = cn.cursor()
            c.execute(sql, (name, description))
            cn.commit()
            status = True
        except:
            cn.rollback()
            log.error(sys.exc_info())
            errors.append({ 'msg': 'An error occured while adding a role.'})

    return {
        'status': status,
        'errors': errors
    }


@view_config(
    route_name='admin_roles_update_command',
    renderer='json',
    request_method='POST'
)
def admin_roles_update_command(request):
    status = False
    errors = []
    data = []
    params = request.json_body

    id = params['id'] if 'id' in params else 0
    active = params['active'] if 'active' in params else False
    name = params['name'] if 'name' in params else ''
    description = params['description'] if 'description' in params else ''
    permissions = params['permissions'] if 'permissions' in params else []

    if id == 0:
        errors.append('Role id is required')
    if name == '':
        errors.append('Role name is required')
    
    if len(errors) == 0:
        # TODO sql
        sql = '''
        update security.roles set
            active = %s,
            name = %s,
            description = %s
        where id = %s
        '''
        cn = Database().get_connection()
        try:
            c = cn.cursor()
            c.execute(sql, (active, name, description, id))

            for p in permissions:
                log.debug(p)
                # TODO sql
                sql = '''
                insert into security.role_permissions values
                (%s,%s)
                on conflict do nothing
                '''
                c.execute(sql, (id, p['id']))

            cn.commit()
            status = True
        except:
            log.error(sys.exc_info())
            cn.rollback()
            errors.append('An error occured while trying to update role')

    return {
        'status': status,
        'errors': errors,
        'data': data
    }


@view_config(
    route_name='admin_roles_role_command',
    renderer='json',
    request_method='POST'
)
def admin_roles_role_command(request):
    status = False
    errors = []
    data = []
    role = {}
    params = request.json_body
    name = params['name'] if 'name' in params else ''
    if name == '':
        errors.append({'msg':'Role names should not be empty'})
    else:
        sql = 'select id, active, created_ts, name, description from security.roles where name = %s'
        cn = Database().get_connection()
        try:
            c = cn.cursor()
            c.execute(sql, (name, ))
            result = c.fetchone()
            role = {
                'id': result[0],
                'active': result[1],
                'created': result[2].isoformat(),
                'name': result[3],
                'description': result[4]
            }

            sql = '''
            select
                p.id,
                p.active,
                p.created_ts,
                p.key,
                p.description
            from security.role_permissions rp
                inner join security.permissions p on rp.permission_id = p.id
            where rp.role_id = %s
            '''
            c.execute(sql, (role['id'], ))
            permissions = []
            for p in c.fetchall():
                permissions.append({
                    'id': p[0],
                    'active': p[1],
                    'created': p[2].isoformat(),
                    'name': p[3],
                    'description': p[4]
                })
            role['permissions'] = permissions
            data.append(role)

            status = True
        except:
            log.error(sys.exc_info())
            errors.append({'msg':'An error occured while retrieving Role'})

    return {
        'status': status,
        'errors': errors,
        'data': data
    }