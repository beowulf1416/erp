# package erp.modules.core.commands.admin
from pyramid.view import view_config
from erp.modules.core.database import Database

import sys
import logging

log = logging.getLogger(__name__)


@view_config(
    route_name='admin_clients_view',
    renderer='json',
    request_method='POST',
    permission='admin.clients.view'
)
def admin_clients_view(request):
    status = False
    errors = []
    data = {}
    clients = []
    sql = '''
    select * from common.clients_get()
    '''

    cn = Database().get_connection('default')
    try:
        c = cn.cursor()
        c.execute(sql)
        c_name = c.fetchone()
        c.execute(f'fetch all in "{c_name[0]}"')
        result = c.fetchall()
        for r in result:
            clients.append({
                'id': r[0],
                'active': r[1],
                'created': r[2].isoformat(),
                'name': r[3],
                'description': r[4]
            })
        data['clients'] = clients
        status = True
    except:
        log.error(sys.exc_info())
        errors.append({ 'type': 'critical', 'message': 'An error occured while trying to retrieve clients' })
    
    return {
        'status': status,
        'errors': errors,
        'data': data
    }


@view_config(
    route_name='admin_client_view',
    renderer='json',
    request_method='POST',
    permission='admin.client.view'
)
def admin_client_view(request):
    status = False
    errors = []
    data = {}
    params = request.json_body
    id = params['id'] if 'id' in params else ''

    if id == '':
        errors.append({ 'type': 'danger', 'message': 'Client name or id is required' })

    if len(errors) == 0:
        sql = 'select * from common.client_get(%s)'
        cn = Database().get_connection('default')
        try:
            c = cn.cursor()
            c.execute(sql, (id, ))
            c_name = c.fetchone()
            c.execute(f'fetch all in "{c_name[0]}"')
            result = c.fetchone()
            data['client'] = {
                'id': result[0],
                'active': result[1],
                'created': result[2].isoformat(),
                'name': result[3],
                'description': result[4]
            }
            status = True
        except:
            log.error(sys.exc_info())
            errors.append({ 'type': 'danger', 'message': 'An error occured while retrieving client information' })

    return {
        'status': status,
        'errors': errors,
        'data': data
    }


@view_config(
    route_name='admin_client_add_view',
    renderer='json',
    request_method='POST',
    permission='admin.clients.add'
)
def admin_client_add_view(request):
    status = False
    errors = []
    data = {}
    params = request.json_body
    params = params['params'] if 'params' in params else params
    name = params['name'] if 'name' in params else ''
    description = params['description'] if 'description' in params else ''

    if name == '':
        errors.append({ 'type': 'danger', 'message': 'Client name is required' })

    if len(errors) == 0:
        sql = 'select * from common.client_add(%s,%s)'
        cn = Database().get_connection('default')
        try:
            c = cn.cursor()
            c.execute(sql, (name, description))
            cn.commit()
            # result = c.fetchall()
            # log.debug(result)
            status = True
        except:
            cn.rollback()
            log.error(sys.exc_info())
            errors.append({ 'type': 'danger', 'message': 'An error occured while trying to add a client' })

    return {
        'status': status,
        'errors': errors,
        'data': data
    }


@view_config(
    route_name='admin_client_update_view',
    renderer='json',
    request_method='POST',
    permission='admin.client.update'
)
def admin_client_update_view(request):
    status = False
    errors = []
    data = {}
    params = request.json_body
    id = params['id'] if 'id' in params else ''
    active = params['active'] if 'active' in params else true
    name = params['name'] if 'name' in params else ''
    description = params['description'] if 'description' in params else ''

    if id == '':
        errors.append({ 'type': 'danger', 'message': 'Client id is required' })

    if name == '':
        errors.append({ 'type': 'danger', 'message': 'Client name is required' })

    if len(errors) == 0:
        sql = 'perform * from common.client_update(%s, %s, %s)'
        cn = Database().get_connection('default')
        try:
            c = cn.cursor()
            c.execute(sql, (id, name, description))
            cn.commit()
            status = True
        except:
            cn.rollback()
            log.error(sys.exc_info())
            errors.append({ 'type': 'danger', 'message': 'An error occured while trying to update Client information' })

    return {
        'status': status,
        'errors': errors,
        'data': data
    }