# package erp.modules.core.commands.admin

import logging

log = logging.getLogger(__name__)

def includeme(config):
    log.debug('including erp.modules.core.commands.admin')

    config.add_route(
        name='admin_home_view',
        path='/admin'
    )

    config.add_route(
        name='admin_params_view',
        path='/admin/*params'
    )

    # clients
    config.add_route(
        name='admin_clients_view',
        path='/api/v1/admin/clients'
    )

    config.add_route(
        name='admin_client_view',
        path='/api/v1/admin/client'
    )

    config.add_route(
        name='admin_client_add_view',
        path='/api/v1/admin/client/add'
    )

    config.add_route(
        name='admin_client_update_view',
        path='/api/v1/admin/client/update'
    )

    # permissions
    config.add_route(
        name='admin_permissions_command',
        path='/api/v1/admin/permissions'
    )

    config.add_route(
        name='admin_permissions_search_command',
        path='/api/v1/admin/permissions/search'
    )

    config.add_route(
        name='admin_permissions_new_command',
        path='/api/v1/admin/permissions/new'
    )

    config.add_route(
        name='admin_permissions_permission_command',
        path='/api/v1/admin/permissions/permission'
    )

    # roles
    config.add_route(
        name='admin_roles_command',
        path='/api/v1/admin/roles'
    )

    config.add_route(
        name='admin_roles_search_command',
        path='/api/v1/admin/roles/search'
    )

    config.add_route(
        name='admin_roles_new_command',
        path='/api/v1/admin/roles/new'
    )

    config.add_route(
        name='admin_roles_update_command',
        path='/api/v1/admin/roles/update'
    )

    config.add_route(
        name='admin_roles_role_command',
        path='/api/v1/admin/roles/role'
    )

    # users

    config.add_route(
        name='admin_users',
        path='/api/v1/admin/users'
    )

    config.add_route(
        name='admin_user',
        path='/api/v1/admin/user'
    )

    config.add_route(
        name='admin_user_update',
        path='/api/v1/admin/user/update'
    )
