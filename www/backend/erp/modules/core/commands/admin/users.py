# package erp.modules.core.commands.admin
from pyramid.view import view_config
import logging
import sys

# from erp.modules.core.authorizationpolicy import is_permitted
from erp.modules.core.database import Database

log = logging.getLogger(__name__)

@view_config(
    route_name='admin_users',
    renderer='json',
    request_method='POST',
    permission='admin.users.view'
)
def admin_users_command(request):
    status = False
    errors = []
    users = []
    # // SQL
    sql = 'select security.get_users()'
    cn = Database().get_connection()
    try:
        c = cn.cursor()
        c.execute(sql)
        c_name = c.fetchone()
        c.execute(f'fetch all in "{c_name[0]}"')
        result = c.fetchall()
        for r in result:
            users.append({
                'id': r[0],
                'active': r[1],
                'created': r[2].isoformat(),
                'email': r[3]
            })
        status = True
    except:
        log.error(sys.exc_info())
        errors.append({ 'msg': 'An error occured while retrieving users' })

    return {
        'status': status,
        'errors': errors,
        'users': users
    }


@view_config(
    route_name='admin_user',
    renderer='json',
    request_method='POST',
    permission='admin.user.view'
)
def admin_users_user_command(request):
    status = False
    errors = []
    data = {}
    user = []
    params = request.json_body
    email = params['email'] if 'email' in params else ''
    if email == '':
        errors.append({'msg': 'Email address is required'})
    else:
        # get user data
        # TODO sql
        sql = 'select id, active, created_ts, email from security.users where email = %s'
        cn = Database().get_connection()
        try:
            c = cn.cursor()
            c.execute(sql, (email, ))
            r = c.fetchone()
            user = {
                'id': r[0],
                'active': r[1],
                'created': r[2].isoformat(),
                'email': r[3]
            }

            # get user roles
            # TODO sql
            sql = '''
            select
                r.id,
                r.active,
                r.created_ts,
                r.name,
                r.description
            from security.roles r
                inner join security.user_roles ur on ur.role_id = r.id
                inner join security.users u on u.id = ur.user_id
            where u.email = %s
            '''
            c.execute(sql, (email, ))
            result = c.fetchall()
            roles = []
            for r in result:
                roles.append({
                    'id': r[0],
                    'active': r[1],
                    'created': r[2].isoformat,
                    'name': r[3],
                    'description': r[4]
                })
            user['roles'] = roles

            # get user client id
            # TODO sql
            sql = 'select * from common.user_clients_get(%s)'
            c.execute(sql, (user['id'], ))
            c_name = c.fetchone()
            c.execute(f'fetch all in "{c_name[0]}"')
            result = c.fetchall()
            clients = []
            for r in result:
                clients.append({
                    'id': r[0],
                    'name': r[3]
                })
            user['clients'] = clients

            data['user'] = user
            status = True
        except:
            log.error(sys.exc_info())
            errors.append({'msg':'An error occured while retrieve user info'})
    
    return {
        'status': status,
        'errors': errors,
        'data': data
    }

@view_config(
    route_name='admin_user_update',
    renderer='json',
    request_method='POST',
    permission='admin.update.user'
)
def admin_users_update_command(request):
    status = False
    errors = []
    data = []
    params = request.json_body

    id = params['id'] if 'id' in params else 0
    active = params['active'] if 'active' in params else False
    email = params['email'] if 'email' in params else ''
    password = params['password'] if 'password' in params else ''
    roles = params['roles'] if 'roles' in params else []

    # TODO sql
    sql = '''
    update security.users set
        email = %s
    where id = %s
    '''
    cn = Database().get_connection()
    try:
        c = cn.cursor()
        c.execute(sql, (email, id))

        for r in roles:
            # TODO sql
            sql = '''
            insert into security.user_roles values 
            (%s, %s)
            '''
            c.execute(sql, (id, r['id']))
        
        cn.commit()
        status = True
    except:
        log.error(sys.exc_info())
        cn.rollback()

    return {
        'status': status,
        'errors': errors,
        'data': data
    }