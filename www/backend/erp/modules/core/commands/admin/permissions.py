# package erp.modules.core.commands.admin
from pyramid.view import view_config
import logging
import sys

from erp.modules.core.database import Database

log = logging.getLogger(__name__)


@view_config(
    route_name='admin_permissions_command',
    renderer='json',
    request_method='POST'
)
def admin_permissions_command(request):
    status = False
    errors = []
    perms = []
    sql = '''
    select
        id,
        active,
        created_ts,
        key,
        description
    from security.permissions
    '''
    cn = Database().get_connection()
    try:
        c = cn.cursor()
        c.execute(sql)
        result = c.fetchall()
        for r in result:
            perms.append({
                'id': r[0],
                'active': r[1],
                'created': r[2].isoformat(),
                'key': r[3],
                'description': r[4]
            })
        status = True
    except:
        log.error(sys.exc_info())
        errors.append({ 'msg': 'An error occured while retrieving permissions' })

    return {
        'status': status,
        'errors': errors,
        'permissions': perms
    }

@view_config(
    route_name='admin_permissions_search_command',
    renderer='json',
    request_method='POST'
)
def admin_permissions_search_command(request):
    status = False
    errors = []
    data = []
    params = request.json_body
    name = params['name'] if 'name' in params else ''
    if name == '':
        errors.append({ 'type': 'warning', 'message': 'Permission name cannot be empty' })
    else:
        # TODO sql
        sql = '''
        select key 
        from security.permissions 
        where key like %s
        '''
        cn = Database().get_connection()
        try:
            c = cn.cursor()
            c.execute(sql, ('%{0}%'.format(name),))
            result = c.fetchall()
            for r in result:
                data.append({
                    'name': r[0]
                })
            status = True
        except:
            log.error(sys.exc_info())
            errors.append({ 'type': 'danger', 'message': 'An error occured while searching for permissions' })

    return {
        'status': status,
        'errors': errors,
        'data': data
    }


@view_config(
    route_name='admin_permissions_new_command',
    renderer='json',
    request_method='POST'
)
def admin_permissions_new_command(request):
    status = False
    errors = []
    data = []
    params = request.json_body
    name = params['name'] if 'name' in params else ''
    description = params['description'] if 'description' in params else ''
    if name == '' or description == '':
        errors.append({ 'msg': 'Role name and description should not be empty.' })
    else:
        # TODO sql
        sql = 'select * from security.permission_add(%s,%s)'
        cn = Database().get_connection()
        try:
            c = cn.cursor()
            c.execute(sql, (name, description))
            data = c.fetchall()
            cn.commit()
            status = True
        except:
            cn.rollback()
            log.error(sys.exc_info())
            errors.append({ 'msg': 'An error occured while adding a permission'})

    return {
        'status': status,
        'errors': errors,
        'data': data
    }

@view_config(
    route_name='admin_permissions_permission_command',
    renderer='json',
    request_method='POST'
)
def admin_permissions_permission_command(request):
    status = False
    errors = []
    messages = []
    data = []
    params = request.json_body
    key = params['key'] if 'key' in params else ''
    if key == '':
        errors.append({'msg': 'Permission key should not be empty'})
    else :
        # TODO sql
        sql = '''
        select id, active, created_ts, key, description 
        from security.permissions 
        where key = %s
        '''
        cn = Database().get_connection()
        try:
            c = cn.cursor()
            c.execute(sql, (key, ))
            result = c.fetchall()
            for r in result:
                data.append({
                    'id': r[0],
                    'active': r[1],
                    'created': r[2].isoformat(),
                    'name': r[3],
                    'description': r[4]
                })
            status = True
        except:
            log.error(sys.exc_info())
            errors.append({'msg':'An error occured while retrieving permission info'})

    return {
        'status': status,
        'errors': errors,
        'messages': messages,
        'data': data
    }