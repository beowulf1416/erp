# package erp.modules.coerce
import logging
from zope.interface import implementer
from pyramid.interfaces import IAuthenticationPolicy

import pyramid.security

log = logging.getLogger(__name__)


@implementer(IAuthenticationPolicy)
class SessionAuthenticationPolicy(object):

    def __init__(self):
        super().__init__()

    def forget(self, request):
        log.debug('forget')

    def remember(self, request):
        log.debug('remember')

    def effective_principals(self, request):
        log.debug('effective_principals')
        # session_id = request.session['session-id']
        # return session_id
        return request.session['session-id']

    def authenticated_userid(self, request):
        log.debug('authenticated_userid')
        # self.session_id = request.session['session-id']
        return request.session['session-id']

    def unauthenticated_userid(self, request):
        log.debug('unauthenticated_userid')
        return request.session['session-id']