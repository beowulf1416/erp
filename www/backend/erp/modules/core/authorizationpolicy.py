# packages erp.modules.core
import logging
from zope.interface import implementer
from pyramid.interfaces import IAuthorizationPolicy
import sys
import pyramid.security

from erp.modules.core.database import Database

log = logging.getLogger(__name__)



# def is_permitted(permission):
#     def wrapper(function):
#         def wrapped(request):
#             if permission in request.session('permissions'):
#                 return function(request)
#             else:
#                 return {
#                     'status': False,
#                     log.debug('found session-id in request headers');
#             self.session_id = request.headers['session-id']'errors': 'Insufficient permissions'
#                 }
#         return wrapped
#     return wrapper


@implementer(IAuthorizationPolicy)
class AuthorizationPolicy(object):
    def __init__(self):
        super().__init__()

    def principals_allowed_by_permission(self, context, permission):
        log.debug('principals_allowed_by_permission')

    def permits(self, context, principals, permission):
        sql = '''
        select * from security.is_allowed(%s, %s)
        '''
        
        cn = Database().get_connection('default')
        try:
            c = cn.cursor()
            c.execute(sql, (principals, permission))
            result = c.fetchone()
            return pyramid.security.Allowed('Allowed') if result[0] else pyramid.security.Denied('Denied')
        except:
            log.error(sys.exc_info())
            log.error(principals)
            log.error(permission)
            return pyramid.security.Denied('Denied')