# package erp.modules.core
import logging

log = logging.getLogger(__name__)

import uuid, base64
import time
import sys
import os
from zope.interface import implementer
from pyramid.interfaces import ISession

from erp.modules.core.database import Database

def db_session_factory():

    @implementer(ISession)
    class Session(dict):
        
        created = time.time()

        def __init__(self, request):
            self.__cn = Database().get_connection('default')
            self.request = request
            log.debug('creating session object')
            if 'session-id' in request.headers:
                log.debug('found session-id in request headers')
                self.session_id = request.headers['session-id']
            else:
                log.debug('generating session id')
                self.session_id = self.generate_session_id()

                user_agent = self.request.user_agent
                language = str(self.request.accept_language)
                remote_addr = self.request.remote_addr

                # create the session
                sql = 'select www.session_create(%s,%s,%s,%s)'
                try:
                    c = self.__cn.cursor()
                    c.execute(sql, (self.session_id, user_agent, language, remote_addr))
                    self.__cn.commit()
                except:
                    self.__cn.rollback()
                    log.error(sys.exc_info())

                self.__setitem__('session-id', self.session_id)

                request.headers['session-id'] = self.session_id

                def set_session_id_callback(request, response):
                    log.debug('setting session id')
                    response.headers['session-id'] = self.session_id

                self.request.add_response_callback(set_session_id_callback)
            
            super().__init__()

        def generate_session_id(self):
            return base64.b64encode(uuid.UUID(bytes = os.urandom(16)).bytes).decode()

        def invalidate(self):
            # log.debug('invalidate')
            sql = 'select * from www.session_destroy(%s)'
            try:
                c = self.__cn.cursor()
                c.execute(sql, (self.session_id, ))
            except:
                log.error(sys.exc_info())

        # dict.__setitem__
        def __setitem__(self, key, value):
            # log.debug('session::__setitem__()')
            # log.debug(key)
            sql = 'select * from www.session_put(%s,%s,%s::text)'
            try:
                c = self.__cn.cursor()
                c.execute(sql,(self.session_id, key, value))
                self.__cn.commit()
            except:
                log.error(sys.exc_info())
                self.__cn.rollback()

        # dict.__getitem__
        def __getitem__(self, key):
            # log.debug('session::__getitem__()')
            # log.debug(key)
            sql = 'select www.session_get(%s,%s)'
            try:
                c = self.__cn.cursor()
                c.execute(sql,(self.session_id, key))
                result = c.fetchone()
                # log.debug(result)
                return result[0]
            except:
                log.error(sys.exc_info())

        def __delitem__(self, key):
            # log.debug('session::__delitem__()')
            sql = 'select * from www.session_remove(%s,%s)'
            try:
                c = self.__cn.cursor()
                c.execute(sql, (self.session_id, key))
                self.__cn.commit()
            except:
                log.error(sys.exc_info())
                self.__cn.rollback()

        def __contains__(self, key):
            # log.debug('session::__contains__()')
            sql = 'select count(*) from www.session_data where session_id = %s and key = %s'
            try:
                c = self.__cn.cursor()
                c.execute(sql, (self.session_id, key))
                result = c.fetchall()
                return result
            except:
                log.debug(sys.exc_info())

    return Session
    