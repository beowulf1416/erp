# package erp.modules.optional.accounting
from pyramid.view import view_config

import sys
import logging

from erp.modules.core.database import Database

log = logging.getLogger(__name__)


@view_config(
    route_name='account_types_view',
    renderer='json',
    request_method='POST'
)
def account_types_view(request):
    status = True
    data = {}
    errors = {}

    sql = 'select * from accounting.account_types()'
    cn = Database().get_connection('default')
    try:
        c = cn.cursor()
        c.execute(sql)
        c_name = c.fetchone()
        c.execute(f'fetch all in "{c_name[0]}"')
        result = c.fetchall()
        account_types = []
        for r in result:
            account_types.append({
                'id': r[0],
                'name': r[1]
            })
        data['account_types'] = account_types
        status = True
    except:
        log.error(sys.exc_info())
        errors.append({ 'type': 'danger', 'message': 'An error occured while retrieving Account Types' })

    return {
        'status': status,
        'data': data,
        'errors': errors
    }


@view_config(
    route_name='accounts_view',
    renderer='json',
    request_method='POST'
)
def accounts_view(request):
    status = True
    data = {}
    errors = {}

    return {
        'status': status,
        'data': data,
        'errors': errors
    }


@view_config(
    route_name='account_view',
    renderer='json',
    request_method='POST',
    permission='accounting.account.view'
)
def account_view(request):
    status = True
    data = {}
    errors = []

    client_id = request.session['client-id']
    params = request.json_body


    return {
        'status': status,
        'data': data,
        'errors': errors
    }


@view_config(
    route_name='account_tree_view',
    renderer='json',
    request_method='POST',
    permission='accounting.account.view'
)
def account_tree_view(request):
    status = False
    data = {}
    errors = []

    client_id = request.session['client-id']
    params = request.json_body
    account_id = params['id'] if 'id' in params else None

    sql = 'select * from accounting.account_tree_get_children(%s,%s)'
    cn = Database().get_connection('default')
    c = cn.cursor()
    try:
        c.execute(sql, (client_id, account_id))
        c_name = c.fetchone()
        c.execute(f'fetch all in "{c_name[0]}"')
        result = c.fetchall()
        accounts = []
        for r in result:
            accounts.append({
                'id': r[0],
                'active': r[1],
                'created': r[2].isoformat(),
                'type_id': r[3],
                'code': r[4],
                'name': r[5],
                'description': r[6]
            })
        data['accounts'] = accounts

        log.debug(accounts)
        log.debug(c.mogrify(sql, (client_id, account_id)))

        status = True
    except:
        log.error(sys.exc_info())
        errors.append({ 'type': 'danger', 'message': 'An error occured while trying to retrieve account children' })

    return {
        'status': status,
        'data': data,
        'errors': errors
    }


@view_config(
    route_name='account_add_view',
    renderer='json',
    request_method='POST',
    permission='accounting.account.add'
)
def account_add_view(request):
    status = False
    data = {}
    errors = []

    client_id = request.session['client-id']

    params = request.json_body['params']
    log.debug(params)

    type_id = params['type_id'] if 'type_id' in params else ''
    code = params['code'] if 'code' in params else ''
    name = params['name'] if 'name' in params else ''
    description = params['description'] if 'description' in params else ''
    parent_account_id = params['parent_account'] if 'parent_account' in params else ''

    if type_id == '':
        errors.append({ 'type': 'warning', 'message': 'Account Type is required' })

    if name == '':
        errors.append({ 'type': 'warning', 'message': 'Account name is required' })

    if len(errors) == 0:
        sql = 'select * from accounting.account_add(%s,%s,%s,%s,%s)'
        cn = Database().get_connection('default')
        try:
            c = cn.cursor()
            c.execute(sql, (client_id, type_id, code, name, description))
            cn.commit()
            result = c.fetchall()
            data['account'] = {
                'id': result[0],
                'name': name
            } 
            status = True
        except:
            cn.rollback()
            log.error(sys.exc_info())
            errors.append({ 'type': 'danger', 'message': 'An error occured while adding an account' })

    return {
        'status': status,
        'data': data,
        'errors': errors
    }