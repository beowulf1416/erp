# package erp.modules.optional.accounting
from pyramid.view import view_config

@view_config(
    route_name='accounting_home_view',
    renderer='erp:templates/index.html.jinja2'
)
@view_config(
    route_name='accounting_params_view',
    renderer='erp:templates/index.html.jinja2'
)
def accounting_view(request):
    return {}