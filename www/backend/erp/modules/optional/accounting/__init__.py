# package erp.modules.optional.accounting
import logging

log = logging.getLogger(__name__)

def includeme(config):
    log.debug('including erp.modules.optional.accounting')

    config.add_route(
        name='accounting_home_view',
        path='/accounting'
    )

    config.add_route(
        name='accounting_params_view',
        path='/accounting/*params'
    )

    # account
    config.add_route(
        name='account_types_view',
        path='/api/v1/accounting/types'
    )

    config.add_route(
        name='accounts_view',
        path='/api/v1/accounting/accounts'
    )

    config.add_route(
        name='account_view',
        path='/api/v1/accounting/account'
    )

    config.add_route(
        name='account_tree_view',
        path='/api/v1/accounting/tree'
    )

    config.add_route(
        name='account_add_view',
        path='/api/v1/accounting/account/add'
    )