# package erp.modules.optional.inventory
from pyramid.view import view_config

@view_config(
    route_name='inventory_home_view',
    renderer='erp:templates/index.html.jinja2'
)
@view_config(
    route_name='inventory_params_view',
    renderer='erp:templates/index.html.jinja2'
)
def inventory_home_view(request):
    return {}