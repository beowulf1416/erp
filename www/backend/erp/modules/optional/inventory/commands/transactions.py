# package erp.modules.optional.inventory.commands
import sys
import logging
from pyramid.view import view_config
from erp.modules.core.database import Database

log = logging.getLogger(__name__)


@view_config(
    route_name='inventory_transactions_view',
    renderer='json',
    request_method='POST'
)
def inventory_transactions_view(request):
    status = False
    data = {}
    errors = []

    return {
        'status': status,
        'data': data,
        'errors': errors
    }


@view_config(
    route_name='inventory_transaction_view',
    renderer='json',
    request_method='POST'
)
def inventory_transaction_view(request):
    status = False
    data = {}
    errors = []

    return {
        'status': status,
        'data': data,
        'errors': errors
    }