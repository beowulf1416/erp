# package erp.modules.optional.inventory.commands
from pyramid.view import view_config
import logging

log = logging.getLogger(__name__)

import erp.modules.optional.inventory.commands.items


def includeme(config):
    log.debug('including erp.modules.optional.inventory.commands')

    # items
    config.add_route(
        name='inventory_items_command',
        path='/api/v1/inventory/items'
    )

    config.add_route(
        name='inventory_item_add_view',
        path='/api/v1/inventory/item/add'
    )

    config.add_route(
        name='inventory_item_view',
        path='/api/v1/inventory/item'
    )

    # transactions
    config.add_route(
        name='inventory_transactions_view',
        path='/api/v1/inventory/transactions'
    )

    config.add_route(
        name='inventory_transaction_view',
        path='/api/v1/inventory/transaction'
    )

    # warehouses
    config.add_route(
        name='inventory_warehouses_view',
        path='/api/v1/inventory/warehouses'
    )

    config.add_route(
        name='inventory_warehouse_add_view',
        path='/api/v1/inventory/warehouses/add'
    )