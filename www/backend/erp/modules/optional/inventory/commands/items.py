# package erp.modules.optional.inventory.commands
from pyramid.view import view_config
import logging
import sys

from erp.modules.core.database import Database

log = logging.getLogger(__name__)


def _result_item_to_dict(r):
    return {
        'id': r[0],
        'active': r[1],
        'created': r[2].isoformat(),
        'name': r[3],
        'description': r[4],
        'brand': r[5],
        'model': r[6],
        'sku': r[7],
        'upc_ean': r[8],
        'uom_id': r[9],
        'perishable': r[10]
    }


@view_config(
    route_name='inventory_items_command',
    renderer='json',
    request_method='POST',
    permission='inventory.items.view'
)
def inventory_items_command(request):
    status = False
    errors = []
    messages = []
    data = {}

    client_id = request.session['client-id']

    params = request.json_body
    search = params['search'] if 'search' in params else {}
    filter = search['filter'] if 'filter' in search else ''
    sort = search['sort'] if 'sort' in search else { 'column': 'name', 'ascending': True }
    pager = search['pager'] if 'pager' in search else { 'current_page': 1, 'items_per_page': 10 }

    column = sort['column'] if 'column' in sort else 'name'
    column = 'name' if column == '' else column 
    order = sort['ascending'] if 'ascending' in sort else True
    order = 'asc' if order else 'desc'

    rows = pager['items_per_page'] if 'items_per_page' in pager else 10
    page = (pager['current_page'] if 'current_page' in pager else 1) - 1

    sort_column = {
        'sku': 'a.sku',
        'name': 'a.name',
        'description': 'a.description'
    }[column]

    sql = '''
    select 
        a.id,
        a.active,
        a.created_ts,
        a.name,
        a.description,
        a.brand,
        a.model,
        a.sku,
        a.upc_ean,
        a.uom_id,
        a.perishable
    from inventory.items a
    where a.client_id = %s
    order by {sort_column} {order}
    limit {items}
    offset {page}
    '''.format(
        sort_column = sort_column,
        order = order,
        items = rows,
        page = rows * page
    ) if filter == '' else '''
    select 
        a.id,
        a.active,
        a.created_ts,
        a.name,
        a.description,
        a.brand,
        a.model,
        a.sku,
        a.upc_ean,
        a.uom_id,
        a.perishable
    from inventory.items a
    where a.client_id = %s
        and (a.sku like %s
            or a.upc_ean like %s
            or a.name like %s
            or a.description like %s)
    order by {sort_column} {order}
    limit {items}
    offset {page}
    '''.format(
        sort_column = sort_column,
        order = order,
        items = rows,
        page = rows * page
    )

    cn = Database().get_connection('default')
    try:
        c = cn.cursor()
        if (filter == ''):
            c.execute(sql, (client_id, ))
        else:
            wildcard = '%{0}%'.format(filter)
            c.execute(sql, (client_id, wildcard, wildcard, wildcard, wildcard))
        result = c.fetchall()
        items = []
        for r in result:
            items.append(_result_item_to_dict(r))
        data['items'] = items
        status = True
    except:
        log.error(sys.exc_info())
        errors.append({ 'type': 'danger', 'message': 'An error occured while trying to retrieve Inventory items' })

    return {
        'status': status,
        'errors': errors,
        'messages': messages,
        'data': data
    }

@view_config(
    route_name='inventory_item_add_view',
    renderer='json',
    request_method='POST',
    permission='inventory.item.add'
)
def inventory_item_add_view(request):
    status = False
    errors = []
    data = {}
    params = request.json_body['params']
    
    client_id = request.session['client-id']
    name = params['name'] if 'name' in params else ''
    description = params['description'] if 'description' in params else ''
    sku = params['sku'] if 'sku' in params else ''
    upc_ean = params['upc_ean'] if 'upc_ean' in params else ''
    perishable = params['perishable'] if 'perishable' in params else ''


    if name == '':
        errors.append({ 'type': 'danger', 'message': 'Inventory item name is required' })

    if len(errors) == 0:
        sql = 'select * from inventory.item_add(%s,%s,%s,%s,%s,%s)'
        cn = Database().get_connection('default')
        try:
            c = cn.cursor()
            c.execute(sql, (client_id, name, description, sku, upc_ean, perishable))
            cn.commit()
            status = True
        except:
            cn.rollback()
            log.error(sys.exc_info())
            errors.append({ 'type': 'danger', 'message': 'An error occured while trying to add an item'})

    return {
        'status': status,
        'errors': errors,
        'data': data
    }


@view_config(
    route_name='inventory_item_view',
    renderer='json',
    request_method='POST'
)
def inventory_item_view(request):
    status = False
    data = {}
    errors = []

    client_id = request.session['client-id']
    params = request.json_body
    item_id = params['id'] if 'id' in params else ''

    sql = '''
    select
        id,
        active,
        created_ts,
        name,
        description,
        brand,
        model,
        sku,
        upc_ean,
        uom_type_id,
        uom_id,
        perishable
    from inventory.items
    where client_id = %s
        and id = %s
    '''
    cn = Database().get_connection('default')

    try:
        c = cn.cursor()
        c.execute(sql, (client_id, item_id))
        r = c.fetchone()
        item = _result_item_to_dict(r)
        data['item'] = item
        status = True
    except:
        log.error(sys.exc_info())
        errors.append({ 'type': 'danger', 'message': 'An error occured while trying to retrieve Inventory Item' })

    return {
        'status': status,
        'errors': errors,
        'data': data
    }