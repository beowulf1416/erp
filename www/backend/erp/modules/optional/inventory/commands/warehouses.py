# package erp.modules.optional.inventory.commands
import sys
import logging

from pyramid.view import view_config
from erp.modules.core.database import Database


log = logging.getLogger(__name__)

@view_config(
    route_name='inventory_warehouses_view',
    renderer='json',
    request_method='POST'
)
def inventory_warehouses_view(request):
    status = False
    data = {}
    errors = []

    return {
        'status': status,
        'data': data,
        'errors': errors
    }

@view_config(
    route_name='inventory_warehouse_add_view',
    renderer='json',
    request_method='POST'
)
def inventory_warehouse_add_view(request):
    status = False
    data = {}
    errors = []

    client_id = request.session['client-id']

    params = request.json_body['params']
    name = params['name'] if 'name' in params else ''
    description = params['description'] if 'description' in params else ''

    if name == '':
        errors.append({ 'type': 'danger', 'message': 'Warehouse name is required' })

    if len(errors) == 0:
        sql = 'select * from inventory.warehouse_add(%s, %s,%s)'
        cn = Database().get_connection('default')
        try:
            c = cn.cursor()
            c.execute(sql, (client_id, name, description))
            cn.commit()
            result = c.fetchone()
            status = True
        except:
            cn.rollback()
            log.error(sys.exc_info())
            errors.append({ 'type': 'danger', 'message': 'An error occured while adding a warehouse' })

    return {
        'status': status,
        'data': data,
        'errors': errors
    }