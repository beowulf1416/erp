# package erp.modules.optional.inventory
import logging

log = logging.getLogger(__name__)


def includeme(config):
    log.debug('erp.modules.optional.inventory included')

    config.add_route(
        name='inventory_home_view',
        path='/inventory'
    )

    config.add_route(
        name='inventory_params_view',
        path='/inventory/*params'
    )